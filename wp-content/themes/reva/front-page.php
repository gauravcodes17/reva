<?php

/***
 * Template Name: Home Page Template
 */
get_header();
?>

<?php if (have_rows('banner_section')) : ?>
    <?php while (have_rows('banner_section')) : the_row(); ?>
        <div class="home-page-banner">
            <div class="overlay"></div>
            <video width="100%" autoplay muted loop playsinline>
                <source src="<?php echo get_sub_field('video'); ?>" type="video/mp4">
                <source src="<?php echo get_template_directory_uri(); ?>/assets/images/home-page-video.ogg" type="video/ogg">
                Your browser does not support the video tag.
            </video>
            <div class="container">
                <div class="row text-center">
                    <div class="col-12">
                        <h1><?php echo get_sub_field('heading'); ?></h1>
                        <span class="line"></span>
                        <p><?php echo get_sub_field('tagline'); ?></p>
                        <span class="home-page-banner-button">
                            <!-- <a href="#">Explore more</a> -->

                            <?php
                            $link = get_sub_field('link');
                            if ($link) :
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                                <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                            <?php endif; ?>

                            <div><img src="<?php echo get_template_directory_uri(); ?>/assets/images/arrow.svg" alt=""></div>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('business_points')) : ?>
    <div class="points">
        <div class="container">
            <div class="row">
                <?php while (have_rows('business_points')) : the_row(); ?>
                    <div class="col-lg-3 col-md-6 col-6">
                        <div class="items">
                            <img src="<?php echo get_sub_field('icon'); ?>" alt="">
                            <p><?php echo get_sub_field('text'); ?></p>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if (have_rows('about_us_section')) : ?>
    <?php while (have_rows('about_us_section')) : the_row(); ?>
        <section class="mt about-us-section">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-12">
                        <div class="p-rel">
                            <img src="<?php echo get_sub_field('image'); ?>" class="img-fluid w-100" alt="">
                            <?php if (have_rows('team_info')) : ?>
                                <div class="about-us-internal-sec">
                                    <?php while (have_rows('team_info')) : the_row(); ?>
                                        <div>
                                            <h5><?php echo get_sub_field('number'); ?></h5>
                                            <p><?php echo get_sub_field('title'); ?></p>
                                        </div>
                                    <?php endwhile; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12">
                        <h6><?php echo get_sub_field('sub_heading'); ?></h6>
                        <h2><?php echo get_sub_field('heading'); ?></h2>
                        <p class="hb"><?php echo get_sub_field('tagline'); ?></p>
                        <p class="nt"><?php echo get_sub_field('content'); ?></p>
                        <?php
                        $link = get_sub_field('link');
                        if ($link) :
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                            <a href="<?php echo esc_url($link_url); ?>" class="primary-button" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="about-us-slider">
                            <?php if (have_rows('logo')) : ?>
                                <?php while (have_rows('logo')) : the_row(); ?>
                                    <div><img src="<?php echo get_sub_field('image'); ?>" class="img-fluid" alt=""></div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('products_section')) : ?>
    <?php while (have_rows('products_section')) : the_row(); ?>
        <section class="mt products-section">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <h5><?php echo get_sub_field('sub_heading'); ?></h5>
                        <h3><?php echo get_sub_field('heading'); ?></h3>
                        <?php
                        $link = get_sub_field('link');
                        if ($link) :
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                            <a href="<?php echo esc_url($link_url); ?>" class="primary-button" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-6">
                        <p><?php echo get_sub_field('tagline'); ?></p>
                        <p><?php echo get_sub_field('content'); ?></p>
                    </div>
                </div>

                <?php
                $args = array(
                    'post_type' => 'products',
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                );

                $loop = new WP_Query($args);
                ?>
                <?php if (!empty($loop)) : ?>
                    <div class="row">
                        <?php while ($loop->have_posts()) : $loop->the_post();  ?>
                            <?php $productId = get_the_ID() ?>
                            <?php $productName = get_the_title(); ?>
                            <div class="col-lg-4 col-md-6 col-12">
                                <div class="p-rel o-hidden">
                                    <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-fluid w-100" alt="<?php echo $productName; ?>">
                                    <div class="p-overlay">
                                        <div class="d-flex justify-content-between align-items-center">
                                            <h4><?php echo $productName; ?></h4>
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/arrow.svg" alt="">
                                        </div>
                                        <p><?php echo get_field('product_description'); ?></p>
                                        <a href="<?php echo get_the_permalink($productId); ?>">Read more</a>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile;
                        wp_reset_postdata(); ?>
                    </div>
                <?php endif; ?>
                <div class="row text-center">
                    <div class="col-12">
                        <span class="home-page-banner-button"><a href="<?php echo get_sub_field('download_catalogue_link'); ?>" target="_blank"><?php echo get_sub_field('download_catalogue'); ?></a>
                            <div><img src="<?php echo get_template_directory_uri(); ?>/assets/images/arrow.svg" alt=""></div>
                        </span>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('innovative_solutions_section')) : ?>
    <?php while (have_rows('innovative_solutions_section')) : the_row(); ?>
        <section class="mt innovative-solutions p-rel">
            <div class="innovative-background-image">
                <img src="<?php echo get_sub_field('innovative_background_image'); ?>" class="img-fluid" alt="">
            </div>
            <div class="container">
                <div class="row text-center">
                    <div class="col-12">
                        <h3><?php echo get_sub_field('heading'); ?></h3>
                    </div>
                </div>
                <div class="row">
                    <?php while (have_rows('innovative')) : the_row(); ?>
                        <div class="col-md-3 col-4">
                            <div class="innovative-items">
                                <img src="<?php echo get_sub_field('icon'); ?>" alt="">
                                <p><?php echo get_sub_field('text'); ?></p>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
            <div class="moving-text">
                <!-- <marquee behavior="smooth" direction="">
                    <?php //while (have_rows('moving_text')) : the_row(); 
                    ?>
                        <span><?php //echo get_sub_field('text'); 
                                ?>,</span>
                    <?php //endwhile; 
                    ?>
                </marquee> -->

                <div class="RightToLeft">
                    <?php while (have_rows('moving_text')) : the_row(); ?>
                        <span><?php echo get_sub_field('text'); ?>,</span>
                    <?php endwhile; ?>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('services_section')) : ?>
    <?php while (have_rows('services_section')) : the_row(); ?>
        <section class="services-section" style="background-image: url(' <?php echo get_sub_field("background_image"); ?>');">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <h5><?php echo get_sub_field('sub_heading'); ?></h5>
                        <h3><?php echo get_sub_field('heading'); ?></h3>
                        <p class="d-md-block d-none"><?php echo get_sub_field('text'); ?></p>
                        <div class="service-buttons d-md-block d-none">
                            <?php
                            $link = get_sub_field('link1');
                            if ($link) :
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                                <a href="<?php echo esc_url($link_url); ?>" class="primary-button" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                            <?php endif; ?>
                            <?php
                            $link = get_sub_field('link2');
                            if ($link) :
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                                <a href="<?php echo esc_url($link_url); ?>" class="secondary-button" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="p-rel">
                            <img src=" <?php echo get_sub_field('service_image'); ?>" class="img-fluid w-100 main-image" alt="">
                            <div class="image-text">
                                <img src="<?php echo get_sub_field('text_icon'); ?>" class="mr-3" alt="">
                                <p><?php echo get_sub_field('image_text'); ?></p>
                            </div>
                        </div>
                        <p class="d-md-none d-block shmt"><?php echo get_sub_field('text'); ?></p>
                        <div class="service-buttons d-md-none d-block">
                            <?php
                            $link = get_sub_field('link1');
                            if ($link) :
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                                <a href="<?php echo esc_url($link_url); ?>" class="primary-button" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                            <?php endif; ?>
                            <?php
                            $link = get_sub_field('link2');
                            if ($link) :
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                                <a href="<?php echo esc_url($link_url); ?>" class="secondary-button d-md-block d-none" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('projects_completed')) : ?>
    <?php while (have_rows('projects_completed')) : the_row(); ?>
        <section class="projects-completed">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-7">
                        <h3><?php echo get_sub_field('heading'); ?></h3>
                    </div>
                    <div class="col-lg-5 p-rel">
                        <h6><?php echo get_sub_field('number_of_projects'); ?></h6>
                        <p><?php echo get_sub_field('tagline'); ?></p>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('case_study')) : ?>
    <section class="case-study-section">
        <div class="row">
            <div class="col-12">
                <div class="case-study-slider">
                    <?php while (have_rows('case_study')) : the_row(); ?>
                        <div class="cs-items p-rel o-hidden">
                            <img src="<?php echo get_sub_field('image'); ?>" class="img-fluid w-100" alt="">
                            <div class="content">
                                <h3><?php echo get_sub_field('number'); ?></h3>
                                <p><?php echo get_sub_field('heading'); ?></p>
                                <?php
                                $link = get_sub_field('link');
                                if ($link) :
                                    $link_url = $link['url'];
                                    $link_title = $link['title'];
                                    $link_target = $link['target'] ? $link['target'] : '_self';
                                ?>
                                    <a href="<?php echo esc_url($link_url); ?>" class="primary-button" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php if (have_rows('usp_section')) : ?>
    <?php while (have_rows('usp_section')) : the_row(); ?>
        <section class="mt usps-section p-rel">
            <div class="usps-bg-image" style="background-image: url(' <?php echo get_sub_field("background_image"); ?>');">
                <!-- <img src="<?php //echo get_sub_field('background_image'); 
                                ?>" class="img-fluid" alt=""> -->
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h5><?php echo get_sub_field('sub_heading'); ?></h5>
                        <h3><?php echo get_sub_field('heading'); ?></h3>
                    </div>
                </div>
                <div class="row">
                    <?php if (have_rows('usp_items')) : ?>
                        <?php while (have_rows('usp_items')) : the_row(); ?>
                            <div class="col-lg-3 col-6">
                                <div class="usps-items p-rel">
                                    <img src="<?php echo get_sub_field('icon'); ?>" class="img-fluid" alt="">
                                    <h4><?php echo get_sub_field('heading'); ?></h4>
                                    <p><?php echo get_sub_field('content'); ?></p>
                                    <img src="<?php echo get_sub_field('background_icon'); ?>" class="img-fluid usps-bg-icon" alt="">
                                </div>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('map_section')) : ?>
    <?php while (have_rows('map_section')) : the_row(); ?>
        <section class="worldwide-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-12">
                        <h3><?php echo get_sub_field('heading'); ?></h3>
                    </div>
                    <div class="col-lg-4 col-12 p-rel">
                        <h6><?php echo get_sub_field('global_numbers'); ?></h6>
                        <p><?php echo get_sub_field('tagliine'); ?></p>
                    </div>
                </div>
            </div>
            <div class="world-map">
                <img src="<?php echo get_sub_field('image'); ?>" class="img-fluid w-100 d-md-block d-none" alt="">
                <div class="country-lists d-md-none d-flex">
                    <?php while (have_rows('country')) : the_row(); ?>
                        <div>
                            <img src="<?php echo get_sub_field('country_icon'); ?>" class="img-fluid w-100" alt="">
                            <span><?php echo get_sub_field('country_name'); ?></span>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('testimonials')) : ?>
    <?php while (have_rows('testimonials')) : the_row(); ?>
        <section class="testimonials">
            <div class="container">
                <div class="row text-center">
                    <div class="col-12">
                        <h5><?php echo get_sub_field('sub_heading'); ?></h5>
                        <h3><?php echo get_sub_field('heading'); ?></h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="testimonial-slider">
                            <?php if (have_rows('items')) : ?>
                                <?php $i = 1; ?>
                                <?php while (have_rows('items')) : the_row(); ?>
                                    <div class="items">
                                        <div class="p-rel">
                                            <img src="<?php echo get_sub_field('thumbnail'); ?>" class="img-fluid w-100" alt="">
                                            <div class="play-icon">
                                                <a href="#videostory<?php echo $i; ?>" class="award-video" href="<?php echo get_sub_field('blog_link'); ?>">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/testimonial-video-play-icon.svg" class="img-fluid w-100" alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <h6><?php echo get_sub_field('name'); ?></h6>
                                        <p><?php echo get_sub_field('designation'); ?></p>
                                    </div>
                                    <?php $i++; ?>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <?php if (have_rows('items')) : ?>
                    <?php $k = 1; ?>
                    <?php while (have_rows('items')) : the_row(); ?>
                        <div class="mfp-hide" id="videostory<?php echo $k; ?>" style="max-width: 100%;margin: 0 auto;display: flex;justify-content: center;">
                            <iframe width="560" height="315" src="<?php echo get_sub_field('youtube_link'); ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                            <button title="Close (Esc)" type="button" class="mfp-close">×</button>
                        </div>
                        <?php $k++; ?>
                    <?php endwhile; ?>
                <?php endif; ?>
                <div class="row mt-5 text-center">
                    <div class="col-12">
                        <?php
                        $link = get_sub_field('page_link');
                        if ($link) :
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                            <a href="<?php echo esc_url($link_url); ?>" class="primary-button" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?> >></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('industry_testimonial')) : ?>
    <?php while (have_rows('industry_testimonial')) : the_row(); ?>
        <section class="industry-testimonial" style="background-image: url(' <?php echo get_sub_field("background_image"); ?>');">
            <div class="container">
                <div class="row text-center">
                    <div class="col-12">
                        <h3><?php echo get_sub_field('heading'); ?></h3>
                    </div>
                </div>
                <div class="row d-flex justify-content-center">
                    <div class="col-md-10 col-12">
                        <div class="industry-slider">
                            <?php if (have_rows('testimonials')) : ?>
                                <?php while (have_rows('testimonials')) : the_row(); ?>
                                    <div class="items">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/quotes.svg" class="img-fluid" alt="quotes">
                                        <p><?php echo get_sub_field('words'); ?></p>
                                        <h5><?php echo get_sub_field('person_details'); ?></h5>
                                        <h6><?php echo get_sub_field('company'); ?></h6>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('fulfilling_section')) : ?>
    <?php while (have_rows('fulfilling_section')) : the_row(); ?>
        <section class="fulfilling-section">
            <div class="container">
                <div class="row col-12">
                    <h3><?php echo get_sub_field('heading'); ?></h3>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('awards_testimonial')) : ?>
    <?php while (have_rows('awards_testimonial')) : the_row(); ?>
        <section class="awards-section o-hidden">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-12">
                        <h3><?php echo get_sub_field('heading'); ?></h3>
                    </div>
                    <div class="col-lg-4 col-12 p-rel">
                        <h6><?php echo get_sub_field('awards_number'); ?></h6>
                        <p class="as-tagline"><?php echo get_sub_field('tagline'); ?></p>
                    </div>
                </div>
            </div><?php if (have_rows('testimonials')) : ?>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="awards-slider">
                                <?php while (have_rows('testimonials')) : the_row(); ?>
                                    <div class="items">
                                        <h4><?php echo get_sub_field('year'); ?></h4>
                                        <img src="<?php echo get_sub_field('image'); ?>" class="img-fluid w-100" alt="awards-image">
                                        <p><?php echo get_sub_field('words'); ?></p>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('blog_section')) : ?>
    <?php while (have_rows('blog_section')) : the_row(); ?>
        <section class="blog-section p-rel o-hidden">
            <div class="blog-bg-img">
                <img src="<?php echo get_sub_field('background_image'); ?>" class="img-fluid w-100" alt="">
            </div>
            <div class="blog-bg-img2">
                <img src="<?php echo get_sub_field('background_image_second'); ?>" class="img-fluid w-100" alt="">
            </div>
            <div class="container">
                <div class="row text-center">
                    <div class="col-12">
                        <h5><?php echo get_sub_field('sub_heading'); ?></h5>
                        <h3><?php echo get_sub_field('heading'); ?></h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="blog-slider">
                            <?php if (have_rows('blog')) : ?>
                                <?php while (have_rows('blog')) : the_row(); ?>
                                    <div class="items">
                                        <div>
                                            <?php if (get_sub_field('date') != "") : ?>
                                                <span><?php echo get_sub_field('date'); ?></span>
                                            <?php endif; ?>
                                            <span><?php echo get_sub_field('category'); ?></span>
                                        </div>
                                        <h4><?php echo get_sub_field('title'); ?></h4>
                                        <?php
                                        $link = get_sub_field('link');
                                        if ($link) :
                                            $link_url = $link['url'];
                                            $link_title = $link['title'];
                                            $link_target = $link['target'] ? $link['target'] : '_self';
                                        ?>
                                            <a href="<?php echo esc_url($link_url); ?>" class="tertiary-button" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?> >></a>
                                        <?php endif; ?>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-12">
                        <?php
                        $link = get_sub_field('all_blog_link');
                        if ($link) :
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                            <a href="<?php echo esc_url($link_url); ?>" class="primary-button" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('call_our_expert_section')) : ?>
    <?php while (have_rows('call_our_expert_section')) : the_row(); ?>
        <section class="call-expert">
            <div class="container">
                <div class="row text-center">
                    <div class="col-12">
                        <h5><?php echo get_sub_field('sub_heading'); ?></h5>
                    </div>
                </div>
                <div class="row gx-0 rowss">
                    <div class="col-lg-7 col-12">
                        <h4><?php echo get_sub_field('heading'); ?></h4>
                        <p><?php echo get_sub_field('tagline'); ?></p>
                        <?php if (have_rows('lists')) : ?>
                            <ul>
                                <?php while (have_rows('lists')) : the_row(); ?>
                                    <li><?php echo get_sub_field('text'); ?></li>
                                <?php endwhile; ?>
                            </ul>
                        <?php endif; ?>
                        <?php
                        $link = get_sub_field('calling_button');
                        if ($link) :
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                            <a href="<?php echo esc_url($link_url); ?>" class="primary-button" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                        <?php endif; ?>
                    </div>
                    <div class="col-lg-5 col-12">
                        <img src="<?php echo get_sub_field('image'); ?>" class="img-fluid w-100" alt="">
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-12">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/call-expert-quotes.svg" class="img-fluid" alt="">
                        <h6><?php echo get_sub_field('quotes'); ?></h6>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>