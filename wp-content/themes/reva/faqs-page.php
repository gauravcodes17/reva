<?php

/***
 * Template Name: FAQs Page Template
 */
get_header();
?>

<?php if (have_rows('banner_section')) : ?>
    <?php while (have_rows('banner_section')) : the_row(); ?>
        <div class="main-banner" style="background-image: url(' <?php echo get_sub_field("background_image"); ?>');">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2><?php echo get_sub_field('heading'); ?></h2>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('faqs_section')) : ?>
    <?php while (have_rows('faqs_section')) : the_row(); ?>
        <section class="mt faqs-section">
            <div class="container">
                <div class="row text-center">
                    <div class="col-12">
                        <h3><?php echo get_sub_field('heading'); ?></h3>
                    </div>
                </div>
                <?php if (have_rows('faqs')) : ?>
                    <div class="accordion" id="accordionExample">
                        <div class="row">
                            <?php $i = 1; ?>
                            <?php while (have_rows('faqs')) : the_row(); ?>
                                <div class="col-md-6 col-12">
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="heading<?php echo $i; ?>">
                                            <button class="accordion-button <?php if($i != 1): ?>collapsed<?php endif; ?>" type="button" data-bs-toggle="collapse" data-bs-target="#collapse<?php echo $i; ?>" aria-expanded="<?php if ($i == 1) : ?>true<?php else : ?>false<?php endif; ?>" aria-controls="collapse<?php echo $i; ?>">
                                                <?php echo get_sub_field('question'); ?>
                                            </button>
                                        </h2>
                                        <div id="collapse<?php echo $i; ?>" class="accordion-collapse collapse <?php if ($i == 1) : ?>show<?php endif; ?>" aria-labelledby="heading<?php echo $i; ?>" data-bs-parent="#accordionExample">
                                            <div class="accordion-body">
                                                <p><?php echo get_sub_field('answer'); ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php $i++ ?>
                            <?php endwhile; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>