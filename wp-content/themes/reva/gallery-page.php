<?php

/***
 * Template Name: Gallery Page Template
 */
get_header();
?>

<?php if (have_rows('banner_section')) : ?>
    <?php while (have_rows('banner_section')) : the_row(); ?>
        <div class="main-banner" style="background-image: url(' <?php echo get_sub_field("background_image"); ?>');">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2><?php echo get_sub_field('heading'); ?></h2>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('gallery_section')) : ?>
    <section class="mt gallery-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <?php $i = 1; ?>
                        <?php while (have_rows('gallery_section')) : the_row(); ?>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link <?php if ($i == 1) : ?>active<?php endif; ?>" id="<?php echo str_replace(' ', '', get_sub_field('tab_heading')); ?>-tab" data-bs-toggle="tab" data-bs-target="#<?php echo str_replace(' ', '', get_sub_field('tab_heading')); ?>" type="button" role="tab" aria-controls="<?php echo get_sub_field('tab_heading'); ?>" aria-selected="<?php if ($i == 1) : ?>true <?php else : ?>false<?php endif; ?>"><?php echo get_sub_field('tab_heading'); ?></button>
                            </li>
                            <?php $i++; ?>
                        <?php endwhile; ?>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <?php $j = 1; ?>
                        <?php while (have_rows('gallery_section')) : the_row(); ?>
                            <div class="tab-pane fade <?php if ($j == 1) : ?>show active<?php endif; ?>" id="<?php echo str_replace(' ', '', get_sub_field('tab_heading')); ?>" role="tabpanel" aria-labelledby="<?php echo str_replace(' ', '', get_sub_field('tab_heading')); ?>-tab">
                                <?php if (have_rows('images')) : ?>
                                    <?php while (have_rows('images')) : the_row(); ?>
                                        <div>
                                            <img src="<?php echo get_sub_field('image'); ?>" class="img-fluid w-100" alt="">
                                        </div>
                                        <?php $j++; ?>
                                    <?php endwhile; ?>
                                <?php endif; ?>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php get_footer(); ?>