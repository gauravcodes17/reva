<section class="blog-posts sidebar">
    <div class="container">
        <?php
        $paged = get_query_var('paged') ? get_query_var('paged') : 1;
        $current_category = get_the_category();
        $args = array(
            'cat' => $current_category[0]->cat_ID,
            'paged' => $paged,
            'post_type'   => 'post',
            'posts_per_page' => 3,
            'post_status' => 'publish',
            'order'    => 'DESC',
            'orderby'    => 'ID',
        );
        $query = new WP_Query($args);

        if ($query->have_posts()) :
        ?>
            <div class="row">
                <?php while ($query->have_posts()) : $query->the_post(); ?>
                    <?php $cats = get_the_category(get_the_ID()); ?>
                    <div class="col-12">
                        <div class="blog-wrap">
                            <div class="blog-content">
                                <div class="mmb d-flex justify-content-between">
                                    <span><img src="<?php echo get_template_directory_uri(); ?>/assets/images/calender.svg" class="img-fluid" alt="date"><?php echo get_the_date('F d, Y'); ?></span>
                                    <span class="category-name"><?php echo $cats[0]->name; ?></span>
                                </div>
                                <a href="<?php echo get_the_permalink(); ?>">
                                    <h5><?php echo get_the_title(); ?></h5>
                                </a>
                                <!-- <p><?php //echo get_field('excerpt_content'); ?></p> -->
                                <a href="<?php echo get_the_permalink(); ?>">Read More >></a>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
        <div class="blog-pagenavi d-none">
            <div class="row text-center">
                <div class="col-md-12">
                    <div class="prev-next">
                        <?php wp_pagenavi(array('query' => $query)); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>