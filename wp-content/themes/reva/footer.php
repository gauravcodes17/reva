<footer class="mt">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-11">
                <div class="row justify-content-between">
                    <div class="col-md-6 col-12">
                        <h4><?php echo get_field('footer_heading', 'option'); ?></h4>
                        <div class="footer-contact-details">
                            <div>
                                <h5><?php echo get_field('country_name', 'option'); ?></h5>
                                <p><?php echo get_field('address', 'option'); ?></p>
                            </div>
                            <div>
                                <h5><?php echo get_field('contact_heading', 'option'); ?></h5>
                                <ul>
                                    <li><a href="tel:<?php echo get_field('contact_no_1', 'option'); ?>"><?php echo get_field('contact_no_1', 'option'); ?></a></li>
                                    <li><a href="tel:<?php echo get_field('contact_no_2', 'option'); ?>"><?php echo get_field('contact_no_2', 'option'); ?></a></li>
                                </ul>
                            </div>
                            <div>
                                <h5><?php echo get_field('social_heading', 'option'); ?></h5>
                                <ul class="footer-social-icons">
                                    <li><a href="<?php echo get_field('twiiter_link', 'option'); ?>" target="_blank"><img src="<?php echo get_field('twitter_icon', 'option'); ?>" class="img-fluid" alt=""></a></li>
                                    <li><a href="<?php echo get_field('facebook_link', 'option'); ?>" target="_blank"><img src="<?php echo get_field('facebook_icon', 'option'); ?>" class="img-fluid" alt=""></a></li>
                                    <li><a href="<?php echo get_field('linkedin_link', 'option'); ?>" target="_blank"><img src="<?php echo get_field('linkedin_icon', 'option'); ?>" class="img-fluid" alt=""></a></li>
                                    <li><a href="<?php echo get_field('instagram_link', 'option'); ?>" target="_blank"><img src="<?php echo get_field('instagram_icon', 'option'); ?>" class="img-fluid" alt=""></a></li>
                                    <li><a href="<?php echo get_field('youtube_link', 'option'); ?>" target="_blank"><img src="<?php echo get_field('youtube_icon', 'option'); ?>" class="img-fluid" alt=""></a></li>
                                </ul>
                            </div>
                            <div>
                                <h5><?php echo get_field('email_heading', 'option'); ?></h5>
                                <ul>
                                    <li><a href="mailto:<?php echo get_field('email_1', 'option'); ?>"><?php echo get_field('email_1', 'option'); ?></a></li>
                                    <li><a href="mailto:<?php echo get_field('email_2', 'option'); ?>"><?php echo get_field('email_2', 'option'); ?></a></li>
                                </ul>
                            </div>
                        </div>
                        <h6 class="since-text-first"><?php echo get_field('founded_year', 'option'); ?></h6>
                    </div>
                    <div class="col-md-5 col-12">
                        <div class="footer-form">
                            <h4>Get in Touch</h4>
                            <?php echo do_shortcode(get_field('footer_form', 'option')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <h6 class="since-text-scnd"><?php echo get_field('founded_year', 'option'); ?></h6>
                        <!-- <div class="map">
                            <iframe src="<?php //echo get_field('footer_map', 'option'); 
                                            ?>" width="100%" height="250" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                        </div> -->
                        <?php if (get_field('footer_map', 'option') != null) : ?>
                            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo get_field('footer_map', 'option'); ?>"></script>
                        <?php else : ?>
                            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6gvzm4-aftiYK68fumTbEc4LXQkEB4A8"></script>
                        <?php endif; ?>

                        <input type="hidden" name="lat" id="lat" value="<?php echo get_field('latitude', 'option'); ?>">
                        <input type="hidden" name="lat" id="long" value="<?php echo get_field('longitude', 'option');  ?>">
                        <?php if (!empty(get_field('latitude', 'option')) && !empty(get_field('longitude', 'option'))) : ?>
                            <section id="map">
                            </section>
                        <?php endif; ?>

                        <script type="text/javascript">
                            if (document.querySelectorAll('#map').length > 0) {
                                // When the window has finished loading create our google map below
                                google.maps.event.addDomListener(window, 'load', init);

                                function init() {
                                    var get_lat, get_long;
                                    if (document.querySelectorAll('#lat').length > 0 && document.querySelectorAll('#long').length > 0) {
                                        get_lat = document.getElementById('lat').value;
                                        get_long = document.getElementById('long').value;
                                    } else {
                                        get_lat = 40.6700; //Default Latitude
                                        get_long = -73.9400; //Default Longitude

                                    }

                                    // Basic options for a simple Google Map
                                    // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                                    var mapOptions = {
                                        // How zoomed in you want the map to start at (always required)
                                        zoom: 14,

                                        // The latitude and longitude to center the map (always required)
                                        center: new google.maps.LatLng(get_lat, get_long), // New York

                                        // How you would like to style the map. 
                                        // This is where you would paste any style found on Snazzy Maps.
                                        styles: [{
                                                "featureType": "all",
                                                "elementType": "labels",
                                                "stylers": [{
                                                    "visibility": "on"
                                                }]
                                            },
                                            {
                                                "featureType": "all",
                                                "elementType": "labels.text.fill",
                                                "stylers": [{
                                                        "saturation": 36
                                                    },
                                                    {
                                                        "color": "#000000"
                                                    },
                                                    {
                                                        "lightness": 40
                                                    }
                                                ]
                                            },
                                            {
                                                "featureType": "all",
                                                "elementType": "labels.text.stroke",
                                                "stylers": [{
                                                        "visibility": "on"
                                                    },
                                                    {
                                                        "color": "#000000"
                                                    },
                                                    {
                                                        "lightness": 16
                                                    }
                                                ]
                                            },
                                            {
                                                "featureType": "all",
                                                "elementType": "labels.icon",
                                                "stylers": [{
                                                    "visibility": "off"
                                                }]
                                            },
                                            {
                                                "featureType": "administrative",
                                                "elementType": "geometry.fill",
                                                "stylers": [{
                                                        "color": "#000000"
                                                    },
                                                    {
                                                        "lightness": 20
                                                    }
                                                ]
                                            },
                                            {
                                                "featureType": "administrative",
                                                "elementType": "geometry.stroke",
                                                "stylers": [{
                                                        "color": "#000000"
                                                    },
                                                    {
                                                        "lightness": 17
                                                    },
                                                    {
                                                        "weight": 1.2
                                                    }
                                                ]
                                            },
                                            {
                                                "featureType": "administrative.country",
                                                "elementType": "labels.text.fill",
                                                "stylers": [{
                                                    "color": "#e5c163"
                                                }]
                                            },
                                            {
                                                "featureType": "administrative.locality",
                                                "elementType": "labels.text.fill",
                                                "stylers": [{
                                                    "color": "#c4c4c4"
                                                }]
                                            },
                                            {
                                                "featureType": "administrative.neighborhood",
                                                "elementType": "labels.text.fill",
                                                "stylers": [{
                                                    "color": "#E66445"
                                                }]
                                            },
                                            {
                                                "featureType": "landscape",
                                                "elementType": "geometry",
                                                "stylers": [{
                                                        "color": "#000000"
                                                    },
                                                    {
                                                        "lightness": 20
                                                    }
                                                ]
                                            },
                                            {
                                                "featureType": "poi",
                                                "elementType": "geometry",
                                                "stylers": [{
                                                        "color": "#000000"
                                                    },
                                                    {
                                                        "lightness": 21
                                                    },
                                                    {
                                                        "visibility": "on"
                                                    }
                                                ]
                                            },
                                            {
                                                "featureType": "poi.business",
                                                "elementType": "geometry",
                                                "stylers": [{
                                                    "visibility": "on"
                                                }]
                                            },
                                            {
                                                "featureType": "road.highway",
                                                "elementType": "geometry.fill",
                                                "stylers": [{
                                                        "color": "#E66445"
                                                    },
                                                    {
                                                        "lightness": "0"
                                                    }
                                                ]
                                            },
                                            {
                                                "featureType": "road.highway",
                                                "elementType": "geometry.stroke",
                                                "stylers": [{
                                                    "visibility": "off"
                                                }]
                                            },
                                            {
                                                "featureType": "road.highway",
                                                "elementType": "labels.text.fill",
                                                "stylers": [{
                                                    "color": "#ffffff"
                                                }]
                                            },
                                            {
                                                "featureType": "road.highway",
                                                "elementType": "labels.text.stroke",
                                                "stylers": [{
                                                    "color": "#e5c163"
                                                }]
                                            },
                                            {
                                                "featureType": "road.arterial",
                                                "elementType": "geometry",
                                                "stylers": [{
                                                        "color": "#000000"
                                                    },
                                                    {
                                                        "lightness": 18
                                                    }
                                                ]
                                            },
                                            {
                                                "featureType": "road.arterial",
                                                "elementType": "geometry.fill",
                                                "stylers": [{
                                                    "color": "#575757"
                                                }]
                                            },
                                            {
                                                "featureType": "road.arterial",
                                                "elementType": "labels.text.fill",
                                                "stylers": [{
                                                    "color": "#ffffff"
                                                }]
                                            },
                                            {
                                                "featureType": "road.arterial",
                                                "elementType": "labels.text.stroke",
                                                "stylers": [{
                                                    "color": "#2c2c2c"
                                                }]
                                            },
                                            {
                                                "featureType": "road.local",
                                                "elementType": "geometry",
                                                "stylers": [{
                                                        "color": "#000000"
                                                    },
                                                    {
                                                        "lightness": 16
                                                    }
                                                ]
                                            },
                                            {
                                                "featureType": "road.local",
                                                "elementType": "labels.text.fill",
                                                "stylers": [{
                                                    "color": "#999999"
                                                }]
                                            },
                                            {
                                                "featureType": "transit",
                                                "elementType": "geometry",
                                                "stylers": [{
                                                        "color": "#000000"
                                                    },
                                                    {
                                                        "lightness": 19
                                                    }
                                                ]
                                            },
                                            {
                                                "featureType": "water",
                                                "elementType": "geometry",
                                                "stylers": [{
                                                        "color": "#000000"
                                                    },
                                                    {
                                                        "lightness": 17
                                                    }
                                                ]
                                            }
                                        ]
                                    };

                                    // Get the HTML DOM element that will contain your map 
                                    // We are using a div with id="map" seen below in the <body>
                                    var mapElement = document.getElementById('map');

                                    // Create the Google Map using our element and options defined above
                                    var map = new google.maps.Map(mapElement, mapOptions);

                                    // Let's also add a marker while we're at it
                                    var marker = new google.maps.Marker({
                                        position: new google.maps.LatLng(get_lat, get_long),
                                        map: map,
                                        title: 'Snazzy!'
                                    });
                                }
                            }
                        </script>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="footer-links">
                            <div class="footer-logo">
                                <img src="<?php echo get_field('footer_logo', 'option'); ?>" class="img-fluid" alt="">
                            </div>
                            <div class="f-links">
                                <?php
                                // Footer Menu
                                if (function_exists('register_primary_menu')) :
                                    wp_nav_menu(array(
                                        'theme_location' => 'Footer',
                                    ));
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="wiw">
    <a href="<?php echo get_field('whatsapp_link', 'option') ?>" aria-describedby="a11y-external-message"><img src="<?php echo get_field('whatsapp_image', 'option') ?>" class="" alt=""></a>
</div>

<div class="sticky-cta">
    <a href="tel:<?php echo get_field('call_us', 'option'); ?>">Call Our Experts</a>
</div>

<!-- Modal -->
<div class="modal fade" id="popup-form-3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Download Your Free PDF <img src="https://uxsingh.com/revacranes/wp-content/uploads/2024/02/icons8-download-48.png" width="24"></h4>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo do_shortcode('[gravityform id="3" title="true" ajax="true"]'); ?>
            </div>
        </div>
    </div>
</div>

<?php
wp_footer();
?>

</body>

</html>