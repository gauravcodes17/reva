<?php

/***
 * Template Name: Training and Workshops Page Template
 */
get_header();
?>

<?php if (have_rows('banner_section')) : ?>
    <?php while (have_rows('banner_section')) : the_row(); ?>
        <div class="main-banner" style="background-image: url(' <?php echo get_sub_field("background_image"); ?>');">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2><?php echo get_sub_field('heading'); ?></h2>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('training_and_workshops_section')) : ?>
    <section class="mt training-and-workshops-section">
        <?php while (have_rows('training_and_workshops_section')) : the_row(); ?>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-12">
                        <h5><?php echo get_sub_field('year'); ?></h5>
                        <?php /*if (have_rows('trainings_and_workshops')) : ?>
                            <?php while (have_rows('trainings_and_workshops')) : the_row(); ?>
                                <h3><?php echo get_sub_field('heading'); ?></h3>
                                <h6><?php echo get_sub_field('tagline'); ?></h6>
                            <?php endwhile; ?>
                        <?php endif;*/ ?>
                    </div>
                    <?php if (get_sub_field('content') != "") : ?>
                        <div class="col-md-6 col-12">
                            <p><?php echo get_sub_field('content'); ?></p>
                        </div>
                    <?php endif; ?>
                </div>
                <?php if (have_rows('trainings_and_workshops')) : ?>
                    <?php while (have_rows('trainings_and_workshops')) : the_row(); ?>
                        <div class="row">
                            <div class="col-12">
                                <h3><?php echo get_sub_field('heading'); ?></h3>
                                <h6><?php echo get_sub_field('tagline'); ?></h6>
                            </div>
                        </div>
                        <?php if (have_rows('images')) : ?>
                            <div class="row images-row">
                                <div class="col-12">
                                    <div class="training-and-workdhops-slider">
                                        <?php while (have_rows('images')) : the_row(); ?>
                                            <div class="items">
                                                <img src="<?php echo get_sub_field('image'); ?>" class="img-fluid w-100" alt="">
                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        <?php endwhile; ?>
    </section>
<?php endif; ?>

<?php if (have_rows('members_cards')) : ?>
    <?php while (have_rows('members_cards')) : the_row(); ?>
        <section class="mt community-member-section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h3><?php echo get_sub_field('heading'); ?></h3>
                    </div>
                </div>
                <?php if (have_rows('cards')) : ?>
                    <div class="row">
                        <?php while (have_rows('cards')) : the_row(); ?>
                            <div class="col-md-6 col-12">
                                <div class="items">
                                    <div>
                                        <?php $rating = get_sub_field('ratings'); ?>
                                        <?php for ($x = 1; $x <= $rating; $x++) {
                                            echo '<svg width="26" height="25" viewBox="0 0 26 25" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M13.5562 0.407993L16.2116 8.67665C16.2834 8.93051 16.5346 9.07558 16.7858 9.07558H25.398C25.9721 9.07558 26.2233 9.8009 25.7568 10.1636L18.7594 15.2771C18.5441 15.4221 18.4723 15.7123 18.5441 15.9299L21.1995 24.1985C21.3789 24.7425 20.7689 25.214 20.3024 24.8513L13.3409 19.7741C13.1256 19.629 12.8385 19.629 12.6591 19.7741L5.6617 24.8876C5.1952 25.214 4.58517 24.7788 4.7646 24.2348L7.42002 15.9661C7.49179 15.7123 7.42002 15.4584 7.20472 15.3133L0.243196 10.1636C-0.223298 9.83716 0.0278908 9.07558 0.602037 9.07558H9.21423C9.46542 9.07558 9.68072 8.89425 9.78837 8.67665L12.4438 0.407993C12.5873 -0.135998 13.3768 -0.135998 13.5562 0.407993Z" fill="#F3B515"/>
</svg> ';
                                        } ?>
                                    </div>
                                    <h6><?php echo get_sub_field('name'); ?></h6>
                                    <p><?php echo get_sub_field('content'); ?></p>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('video_feedbacks_section')) : ?>
    <?php while (have_rows('video_feedbacks_section')) : the_row(); ?>
        <section class="mt video-feedbacks-section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h3><?php echo get_sub_field('heading'); ?></h3>
                    </div>
                </div>
                <?php if (have_rows('video_feedback')) : ?>
                    <div class="row">
                        <div class="col-12">
                            <div class="training-video-feedbacks-slider">
                                <?php $i = 1; ?>
                                <?php while (have_rows('video_feedback')) : the_row(); ?>
                                    <div class="items">
                                        <div class="p-rel">
                                            <img src="<?php echo get_sub_field('image'); ?>" class="img-fluid w-100" alt="">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/youtube-icon.png" class="img-fluid yi" alt="">
                                        </div>
                                        <a href="#t-story<?php echo $i; ?>" class="award-video" href="<?php echo get_sub_field('video_link'); ?>">
                                            <?php echo get_sub_field('text'); ?>
                                        </a>
                                    </div>
                                    <?php $i++; ?>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    </div>
                    <?php $k = 1; ?>
                    <?php while (have_rows('video_feedback')) : the_row(); ?>
                        <div class="mfp-hide" id="t-story<?php echo $k; ?>" style="max-width: 100%;margin: 0 auto;display: flex;justify-content: center;">
                            <iframe width="560" height="315" src="<?php echo get_sub_field('video_link'); ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                            <button title="Close (Esc)" type="button" class="mfp-close">×</button>
                        </div>
                        <?php $k++; ?>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>