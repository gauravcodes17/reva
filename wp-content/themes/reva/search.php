<?php

/* Template Name: SearchWP Results */

get_header();

$s = get_query_var('s');
// print_r($s);
?>

<!---------- Blogs Section ---------->

<div class="blog-posts bpmt">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-12">
                <div class="main-blog-wrapper">
                    <h1><?php if (!empty($s)) : echo 'Search for ' . '"' . $s . '"';
                        else : ?> No Search Query Found <?php endif; ?></h1>
                    <?php
                    $paged = get_query_var('paged') ? get_query_var('paged') : 1;
                    $args = array(
                        'paged' => $paged,
                        'post_type'   => 'post',
                        's' => $s,
                        'posts_per_page' => 8,
                        'post_status' => 'publish',
                        'order'    => 'DESC',
                        'orderby'    => 'ID',
                        // 'search_filter_id' => 975,
                    );
                    $query = new WP_Query($args);

                    if ($query->have_posts()) :
                    ?>
                        <div class="row">
                            <?php while ($query->have_posts()) : $query->the_post(); ?>
                                <?php $cats = get_the_category(get_the_ID()); ?>
                                <div class="col-lg-6 col-12">
                                    <div class="blog-wrap">
                                        <div class="blog-content">
                                            <div class="mmb d-flex justify-content-between">
                                                <span><img src="<?php echo get_template_directory_uri(); ?>/assets/images/calender.svg" class="img-fluid" alt="date"><?php echo get_the_date('F d, Y'); ?></span>
                                                <span class="category-name"><?php echo $cats[0]->name; ?></span>
                                            </div>
                                            <a href="<?php echo get_the_permalink(); ?>">
                                                <h5><?php echo get_the_title(); ?></h5>
                                            </a>
                                            <p><?php echo get_field('excerpt_content'); ?></p>
                                            <a href="<?php echo get_the_permalink(); ?>">Read More >></a>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile; ?>

                        </div>
                    <?php else : ?>
                        <div class="row mt-4">
                            <div class="col-12">
                                <h4>No Search Result Found !</h4>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="blog-pagenavi">
                    <div class="row text-center">
                        <div class="col-md-12">
                            <div class="prev-next">
                                <?php wp_pagenavi(array('query' => $query)); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-12">
                <div class="blog-right-section">
                    <div class="categories">
                        <?php dynamic_sidebar('blog-categories-sidebar'); ?>
                    </div>
                    <div class="recent-post">
                        <h4>Inspiration</h4>
                        <?php //dynamic_sidebar('recent-post-sidebar'); 
                        ?>
                        <?php get_template_part('template-parts/inspiration-sidebar'); ?>
                    </div>
                    <div class="recent-post">
                        <h4>Recent News</h4>
                        <?php //dynamic_sidebar('recent-post-sidebar'); 
                        ?>
                        <?php get_template_part('template-parts/recent-post-sidebar'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!---------- Blogs End ---------->

<?php
get_footer();
?>