<?php

/***
 * Template Name: Contact Us Page Template
 */
get_header();
?>

<?php if (have_rows('banner_section')) : ?>
    <?php while (have_rows('banner_section')) : the_row(); ?>
        <div class="main-banner" style="background-image: url(' <?php echo get_sub_field("background_image"); ?>');">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2><?php echo get_sub_field('heading'); ?></h2>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('contact_details')) : ?>
    <?php while (have_rows('contact_details')) : the_row(); ?>
        <section class="mt contact-details">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-12">
                        <img src="<?php echo get_sub_field('image'); ?>" class="img-fluid" alt="">
                    </div>
                    <div class="col-lg-6 col-12">
                        <h3><?php echo get_sub_field('main_heading'); ?></h3>
                        <h5><?php echo get_sub_field('sub_heading'); ?></h5>
                        <?php if (have_rows('details')) : ?>
                            <?php while (have_rows('details')) : the_row(); ?>
                                <div class="details">
                                    <h6><?php echo get_sub_field('name'); ?></h6>
                                    <div>
                                        <span class="mail">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/mail.svg" class="img-fluid" alt="mail">
                                            <a href="mailto:<?php echo get_sub_field('email'); ?>"><?php echo get_sub_field('email'); ?></a>
                                        </span>
                                        <span class="phone">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/phone.svg" class="img-fluid" alt="phone">
                                            <?php echo get_sub_field('phone'); ?>
                                        </span>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>