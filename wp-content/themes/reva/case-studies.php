<?php

/***
 * Template Name: Case Study Page Template
 */
get_header();
?>

<?php if (have_rows('banner_section')) : ?>
    <?php while (have_rows('banner_section')) : the_row(); ?>
        <div class="main-banner" style="background-image: url(' <?php echo get_sub_field("background_image"); ?>');">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2><?php echo get_sub_field('heading'); ?></h2>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('case_study_content')) : ?>
    <?php while (have_rows('case_study_content')) : the_row(); ?>
        <section class="mt cs-content-section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <?php if (get_sub_field('sub_heading') != "") : ?>
                            <h5 class="sub-heading"><?php echo get_sub_field('sub_heading'); ?></h5>
                        <?php endif; ?>
                        <?php if (get_sub_field('heading') != "") : ?>
                            <h3 class="heading"><?php echo get_sub_field('heading'); ?></h3>
                        <?php endif; ?>
                        <?php if (get_sub_field('image') != "") : ?>
                            <img src="<?php echo get_sub_field('image'); ?>" class="img-fluid w-100" alt="">
                        <?php endif; ?>
                        <?php if (get_sub_field('para') != "") : ?>
                            <p class="para"><?php echo get_sub_field('para'); ?></p>
                        <?php endif; ?>
                        <?php if (have_rows('project_challanges')) : ?>
                            <?php while (have_rows('project_challanges')) : the_row(); ?>
                                <?php if (get_sub_field('project_challange_content') != "") : ?>
                                    <div class="projects-challanges <?php if (get_sub_field('with_background') == "On") : ?> wb <?php endif; ?>">
                                        <?php echo get_sub_field('project_challange_content'); ?>
                                    </div>
                                <?php endif; ?>
                            <?php endwhile; ?>
                        <?php endif; ?>
                        <div class="content-1">
                            <?php echo get_sub_field('content_1'); ?>
                        </div>
                        <?php if (get_sub_field('full_image') != "") : ?>
                            <img src=" <?php echo get_sub_field('full_image'); ?>" class="img-fluid w-100" alt="">
                        <?php endif; ?>
                        <?php if (have_rows('half_image')) : ?>
                            <?php while (have_rows('half_image')) : the_row(); ?>
                                <div class="hi d-flex justify-content-between">
                                    <?php if (get_sub_field('image_one') != "") : ?>
                                        <img src=" <?php echo get_sub_field('image_one'); ?>" class="img-fluid hi-inner-img" alt="">
                                    <?php endif; ?>
                                    <?php if (get_sub_field('image_two') != "") : ?>
                                        <img src=" <?php echo get_sub_field('image_two'); ?>" class="img-fluid hi-inner-img" alt="">
                                    <?php endif; ?>
                                </div>
                            <?php endwhile; ?>
                        <?php endif; ?>
                        <div class="content-2">
                            <?php echo get_sub_field('content_2'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('impact_section')) : ?>
    <?php while (have_rows('impact_section')) : the_row(); ?>
        <?php if (get_sub_field('heading') != "") : ?>
            <div class="impact-section">
                <div class="container">
                    <div class="row justify-content-between">
                        <div class="col-lg-7 col-md-7 col-12">
                            <h4><?php echo get_sub_field('heading'); ?></h4>
                            <div class="results">
                                <p><?php echo get_sub_field('results'); ?></p>
                                <h5><?php echo get_sub_field('impact_in_numbers'); ?></h5>
                                <h6><?php echo get_sub_field('tagline'); ?></h6>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5 col-12">
                            <?php if (have_rows('numbers')) : ?>
                                <div class="impact-numbers p-rel">
                                    <?php while (have_rows('numbers')) : the_row(); ?>
                                        <div>
                                            <h6><?php echo get_sub_field('number'); ?></h6>
                                            <p><?php echo get_sub_field('text'); ?></p>
                                        </div>
                                    <?php endwhile; ?>
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/numbers-arrow.svg" class="img-fluid" alt="">
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>