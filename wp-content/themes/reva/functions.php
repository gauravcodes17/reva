<?php

/**
 * Functions and definitions
 */


// Exit-if accessed directly
if (!defined('ABSPATH'))
    exit;


/* THEME OPTIONS PAGE - HEADER, FOOTER, FAQS, CONSULTATION */
if (function_exists('acf_add_options_page')) {

    acf_add_options_page(
        array(
            'page_title' => 'Theme Options',
            'menu_title' => 'Theme Options',
            'menu_slug' => 'theme-options',
            'capability' => 'edit_posts',
            'parent_slug' => '',
            'position' => false,
            'icon_url' => false
        )
    );

    acf_add_options_sub_page(
        array(
            'page_title' => 'Header',
            'menu_title' => 'Header',
            'menu_slug' => 'theme-options-header',
            'capability' => 'edit_posts',
            'parent_slug' => 'theme-options',
            'position' => false,
            'icon_url' => false
        )
    );

    acf_add_options_sub_page(
        array(
            'page_title' => 'Footer',
            'menu_title' => 'Footer',
            'menu_slug' => 'theme-options-footer',
            'capability' => 'edit_posts',
            'parent_slug' => 'theme-options',
            'position' => false,
            'icon_url' => false
        )
    );

    acf_add_options_sub_page(
        array(
            'page_title' => 'Common Sections',
            'menu_title' => 'Common Sections',
            'menu_slug' => 'theme-options-common',
            'capability' => 'edit_posts',
            'parent_slug' => 'theme-options',
            'position' => false,
            'icon_url' => false
        )
    );

    acf_add_options_sub_page(
        array(
            'page_title' => 'Error',
            'menu_title' => 'Error',
            'menu_slug' => 'theme-options-error',
            'capability' => 'edit_posts',
            'parent_slug' => 'theme-options',
            'position' => false,
            'icon_url' => false,
            'post_id' => 'error',
        )
    );
}

function add_theme_scripts()
{

    /* Styles */
    wp_enqueue_style('font', get_template_directory_uri() . '/assets/css/font.css', false, '1.0', 'all');
    wp_enqueue_style('common-style', get_template_directory_uri() . '/assets/css/common.css', false, '1.0', 'all');
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', false, '1.0', 'all');
    wp_enqueue_style('home', get_template_directory_uri() . '/assets/css/home-page.css', false, '1.0', 'all');
    wp_enqueue_style('about-us', get_template_directory_uri() . '/assets/css/about-us-page.css', false, '1.0', 'all');
    wp_enqueue_style('services', get_template_directory_uri() . '/assets/css/services-page.css', false, '1.0', 'all');
    wp_enqueue_style('case-studies', get_template_directory_uri() . '/assets/css/case-studies-page.css', false, '1.0', 'all');
    wp_enqueue_style('industry', get_template_directory_uri() . '/assets/css/industry-page.css', false, '1.0', 'all');
    wp_enqueue_style('contact', get_template_directory_uri() . '/assets/css/contact-page.css', false, '1.0', 'all');
    wp_enqueue_style('career', get_template_directory_uri() . '/assets/css/career-page.css', false, '1.0', 'all');
    wp_enqueue_style('training-workshops', get_template_directory_uri() . '/assets/css/training-workshops-page.css', false, '1.0', 'all');
    wp_enqueue_style('gallery', get_template_directory_uri() . '/assets/css/gallery-page.css', false, '1.0', 'all');
    wp_enqueue_style('faqs', get_template_directory_uri() . '/assets/css/faqs-page.css', false, '1.0', 'all');
    wp_enqueue_style('blog', get_template_directory_uri() . '/assets/css/blog-page.css', false, '1.0', 'all');
    wp_enqueue_style('crane-maintenance', get_template_directory_uri() . '/assets/css/crane-maintenance.css', false, '1.0', 'all');
    wp_enqueue_style('product', get_template_directory_uri() . '/assets/css/product-page.css', false, '1.0', 'all');
    wp_enqueue_style('slick', get_template_directory_uri() . '/assets/css/slick.css', false, '1.0', 'all');
    wp_enqueue_style('slick-theme', get_template_directory_uri() . '/assets/css/slick-theme.css', false, '1.0', 'all');
    wp_enqueue_style('magnific', get_template_directory_uri() . '/assets/css/magnific-popup.css', false, '1.0', 'all');

    /* Scripts */

    if (!is_admin()) {
        //Call JQuery
        //wp_deregister_script('jquery');
    }
    wp_enqueue_script('jquery', get_template_directory_uri() . '/assets/js/jquery.js', array(), null, true);
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array(), null, true);
    wp_enqueue_script('slick', get_template_directory_uri() . '/assets/js/slick.min.js', array(), null, true);
    wp_enqueue_script('main', get_template_directory_uri() . '/assets/js/main.js', array(), null, true);
    wp_enqueue_script('magnific', get_template_directory_uri() . '/assets/js/magnific-popup.js', array(), null, true);
    wp_enqueue_script('map-js', get_template_directory_uri() . 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCkuZDoAUVXqbZSyBBW0JUDJebdovUbiIk', array(), null, true);
    
}
add_action('wp_enqueue_scripts', 'add_theme_scripts');

// To allow svg file upload in admin
function cc_mime_types($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/* REGISTER PRIMARY MENUS */
add_action('after_setup_theme', 'register_primary_menu');
function register_primary_menu()
{
    register_nav_menu('TopBar', __('TopBar Menu', 'reva'));
    register_nav_menu('Header', __('Header Menu', 'reva'));
    register_nav_menu('Footer', __('Footer Menu', 'reva'));
}

// Add Theme Support
add_theme_support('post-thumbnails');

/*Set post views count using post meta*/
function setPostViews($postID)
{
    $countKey = 'post_views_count';
    $count = get_post_meta($postID, $countKey, true);
    if ($count == '') {
        $count = 0;
        delete_post_meta($postID, $countKey);
        add_post_meta($postID, $countKey, '0');
    } else {
        $count++;
        update_post_meta($postID, $countKey, $count);
    }
}

function reva_init()
{
    register_sidebar(
        array(
            'name' => esc_html__('Blog Categories Sidebar', 'reva'),
            'id' => 'blog-categories-sidebar',
            'description' => esc_html__('Add widgets here to appear in details blog page.', 'reva'),
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '',
            'after_title' => '',
        )
    );

    register_sidebar(
        array(
            'name' => esc_html__('Recent Post Sidebar', 'reva'),
            'id' => 'recent-post-sidebar',
            'description' => esc_html__('Add widgets here to appear in details blog page.', 'reva'),
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '',
            'after_title' => '',
        )
    );

    register_sidebar(
        array(
            'name' => esc_html__('Product Sidebar', 'reva'),
            'id' => 'product-sidebar',
            'description' => esc_html__('Add widgets here to appear in details product page.', 'reva'),
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '',
            'after_title' => '',
        )
    );

    register_sidebar(
        array(
            'name' => esc_html__('Blog Search Form Sidebar', 'reva'),
            'id' => 'blog-search-form-sidebar',
            'description' => esc_html__('Add widgets here to appear in details product page.', 'reva'),
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '',
            'after_title' => '',
        )
    );
}
add_action('widgets_init', 'reva_init');

add_filter('nav_menu_css_class', 'special_nav_class', 10, 2);

function special_nav_class($classes, $item)
{
    if (in_array('current-menu-item', $classes)) {
        $classes[] = 'active-menu-link ';
    }
    return $classes;
}

// add_filter( 'gform_form_args', 'setup_form_args' );
// function setup_form_args( $form_args ) {
//     $form_args['ajax'] = true;
 
//     return $form_args;
// }