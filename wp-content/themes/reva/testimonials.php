<?php

/***
 * Template Name: Testimonial Page Template
 */
get_header();
?>

<?php if (have_rows('banner_section')) : ?>
    <?php while (have_rows('banner_section')) : the_row(); ?>
        <div class="main-banner" style="background-image: url(' <?php echo get_sub_field("background_image"); ?>');">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2><?php echo get_sub_field('heading'); ?></h2>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('testimonials')) : ?>
    <?php while (have_rows('testimonials')) : the_row(); ?>
        <section class="testimonials tp">
            <div class="container">
                <div class="row text-center">
                    <div class="col-12">
                        <h5><?php echo get_sub_field('sub_heading'); ?></h5>
                        <h3><?php echo get_sub_field('heading'); ?></h3>
                    </div>
                </div>
                <?php if (have_rows('items')) : ?>
                    <?php $i = 1; ?>
                    <div class="row rg">
                        <?php while (have_rows('items')) : the_row(); ?>
                            <div class="col-lg-4 col-md-6 col-12">
                                <div class="items p-rel">
                                    <div class="p-rel">
                                        <img src="<?php echo get_sub_field('thumbnail'); ?>" class="img-fluid w-100" alt="">
                                        <div class="play-icon">
                                            <a href="#videostory<?php echo $i; ?>" class="award-video" href="<?php echo get_sub_field('blog_link'); ?>">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/testimonial-video-play-icon.svg" class="img-fluid w-100" alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <h6><?php echo get_sub_field('name'); ?></h6>
                                    <p><?php echo get_sub_field('designation'); ?></p>
                                </div>
                            </div>
                            <?php $i++; ?>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>
                <?php if (have_rows('items')) : ?>
                    <?php $k = 1; ?>
                    <?php while (have_rows('items')) : the_row(); ?>
                        <div class="mfp-hide" id="videostory<?php echo $k; ?>" style="max-width: 100%;margin: 0 auto;display: flex;justify-content: center;">
                            <iframe width="560" height="315" src="<?php echo get_sub_field('youtube_link'); ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                            <button title="Close (Esc)" type="button" class="mfp-close">×</button>
                        </div>
                        <?php $k++; ?>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>