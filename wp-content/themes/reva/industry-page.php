<?php

/***
 * Template Name: Industry Page Template
 */
get_header();
?>

<?php if (have_rows('banner_section')) : ?>
    <?php while (have_rows('banner_section')) : the_row(); ?>
        <div class="main-banner" style="background-image: url(' <?php echo get_sub_field("background_image"); ?>');">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2><?php echo get_sub_field('heading'); ?></h2>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>

<section class="mt all-industries">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex">
                <span class="line"></span>
                <h3>Top industries which we cater to</h3>
            </div>
        </div>
        <?php
        $args = array(
            'post_type' => 'industry',
            'post_status' => 'publish',
            'posts_per_page' => -1,
        );

        $loop = new WP_Query($args);
        ?>
        <?php if (!empty($loop)) : ?>
            <div class="row">
                <?php while ($loop->have_posts()) : $loop->the_post();  ?>
                    <?php $industryId = get_the_ID() ?>
                    <?php $industryName = get_the_title(); ?>
                    <div class="col-md-4 col-12">
                        <div class="industries p-rel">
                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-fluid w-100" alt="<?php echo $industryName; ?>">
                            <p><?php echo $industryName; ?></p>
                        </div>
                        <div class="text-center">
                            <a href="<?php echo get_the_permalink($industryId); ?>" class="primary-button">Read More</a>
                        </div>
                    </div>
                <?php endwhile;
                wp_reset_postdata(); ?>
            </div>
        <?php endif; ?>
    </div>
</section>

<?php if (have_rows('other_industries')) : ?>
    <?php while (have_rows('other_industries')) : the_row(); ?>
        <section class="mt other-industries">
            <div class="container">
                <div class="row">
                    <div class="col-12 d-flex">
                        <span class="line"></span>
                        <h3><?php echo get_sub_field('heading'); ?></h3>
                    </div>
                </div>
                <?php if (have_rows('industries')) : ?>
                    <div class="row">
                        <?php while (have_rows('industries')) : the_row(); ?>
                            <div class="col-lg-3 col-md-6 col-6">
                                <div class="items p-rel">
                                    <img src="<?php echo get_sub_field('image'); ?>" class="img-fluid w-100" alt="">
                                    <p><?php echo get_sub_field('title'); ?></p>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>