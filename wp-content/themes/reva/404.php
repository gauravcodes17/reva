<?php

/***
 * Template Name: Error Page Template
 */

get_header();
?>

<div class="thankyou-banner">
    <div class="container">
        <div class="row text-center">
            <div class="col-12">
                <i class="fa-solid fa-circle-exclamation"></i>
                <h1><?php echo get_field('text', 'error'); ?></h1>
                <a href="<?php echo get_field('link', 'error'); ?>" class="button-tertiary">Go Back To Home Page</a>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>