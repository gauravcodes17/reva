<?php

/***
 * Template Name: Services Page Template
 */
get_header();
?>

<?php if (have_rows('banner_section')) : ?>
    <?php while (have_rows('banner_section')) : the_row(); ?>
        <div class="main-banner p-rel" style="background-image: url(' <?php echo get_sub_field("background_image"); ?>');">
            <div class="container z-1">
                <div class="row">
                    <div class="col-12">
                        <h2><?php echo get_sub_field('heading'); ?></h2>
                    </div>
                </div>
            </div>
            <div class="new-overlay"></div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('services_section')) : ?>
    <?php while (have_rows('services_section')) : the_row(); ?>
        <section class="mt services-page-section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <p><?php echo get_sub_field('content'); ?></p>
                    </div>
                </div>
                <?php if (have_rows('services')) : ?>
                    <div class="row">
                        <?php while (have_rows('services')) : the_row(); ?>
                            <div class="col-lg-3 col-md-6 col-12">
                                <img src="<?php echo get_sub_field('image'); ?>" class="img-fluid w-100" alt="">
                                <h4><?php echo get_sub_field('service_name'); ?></h4>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('contact_details')) : ?>
    <?php while (have_rows('contact_details')) : the_row(); ?>
        <section class="mt contact-details-section">
            <div class="container">
                <div class="row gx-0">
                    <div class="col-md-3 col-12">
                        <img src="<?php echo get_sub_field('image'); ?>" class="img-fluid w-100" alt="">
                    </div>
                    <div class="col-md-6 col-12">
                        <h4><?php echo get_sub_field('heading'); ?></h4>
                        <div>
                            <p><?php echo get_sub_field('call_us_heading'); ?>: <a href="tel:<?php echo get_sub_field('number'); ?>"><?php echo get_sub_field('number'); ?></a></p>
                            <p><?php echo get_sub_field('mail_us_heading'); ?>: <a href="mailto:<?php echo get_sub_field('email'); ?>"><?php echo get_sub_field('email'); ?></a></p>
                        </div>
                    </div>
                    <div class="col-md-3 col-12">
                        <?php
                        $link = get_sub_field('button');
                        if ($link) :
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                            <a href="<?php echo esc_url($link_url); ?>" class="secondary-button" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>