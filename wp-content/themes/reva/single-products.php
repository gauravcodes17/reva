<?php

/***
 * Template Name: Product Page Template
 */
get_header();
?>

<?php if (have_rows('banner_section')) : ?>
    <?php while (have_rows('banner_section')) : the_row(); ?>
        <div class="main-banner" style="background-image: url(' <?php echo get_sub_field("background_image"); ?>');">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2><?php echo get_sub_field('heading'); ?></h2>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>

<section class="mt product-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-12 sb">
                <?php dynamic_sidebar('product-sidebar'); ?>
                <?php if (have_rows('free_quote')) : ?>
                    <?php while (have_rows('free_quote')) : the_row(); ?>
                        <?php if (get_sub_field('catalogue_heading') != "") : ?>
                            <div class="col-fq p-rel">
                                <div class="">
                                    <h5><?php echo get_sub_field('catalogue_heading'); ?></h5>
                                    <p><?php echo get_sub_field('catalogue_text'); ?></p>
                                    <?php
                                    $link = get_sub_field('catalogue_download_link');
                                    if ($link) :
                                        $link_url = $link['url'];
                                        $link_title = $link['title'];
                                        $link_target = $link['target'] ? $link['target'] : '_self';
                                    ?>
                                        <a href="<?php echo esc_url($link_url); ?>" class="primary-button" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <?php if (have_rows('product_details')) : ?>
                <?php while (have_rows('product_details')) : the_row(); ?>
                    <div class="col-lg-8 col-12">
                        <div class="product-details">
                            <?php if (get_sub_field('product_image') != "") : ?>
                                <img src="<?php echo get_sub_field('product_image'); ?>" class="img-fluid w-100" alt="">
                            <?php endif; ?>
                            <p><?php echo get_sub_field('content'); ?></p>
                            <div class="two-column">
                                <div class="row align-items-center">
                                    <?php if (have_rows('benefits')) : ?>
                                        <?php while (have_rows('benefits')) : the_row(); ?>
                                            <?php if (get_sub_field('heading') != "") : ?>
                                                <div class="col-md-7">
                                                    <h5><?php echo get_sub_field('heading'); ?></h5>
                                                    <?php if (have_rows('lists')) : ?>
                                                        <ul>
                                                            <?php while (have_rows('lists')) : the_row(); ?>
                                                                <li><?php echo get_sub_field('text'); ?></li>
                                                            <?php endwhile; ?>
                                                        </ul>
                                                    <?php endif; ?>
                                                </div>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                    <?php endif; ?>
                                    <?php if (have_rows('catalogue')) : ?>
                                        <?php while (have_rows('catalogue')) : the_row(); ?>
                                            <?php if (get_sub_field('catalogue_image') != "") : ?>
                                                <div class="col-md-5">
                                                    <div class="p-rel">
                                                        <?php if (get_sub_field('catalogue_image') != "") : ?>
                                                            <img src="<?php echo get_sub_field('catalogue_image'); ?>" class="img-fluid w-100" alt="">
                                                        <?php endif; ?>
                                                        <div class="p-abs cus-content">
                                                            <?php if (get_sub_field('catalogue_heading') != "") : ?>
                                                                <h5><?php echo get_sub_field('catalogue_heading'); ?></h5>
                                                            <?php endif; ?>
                                                            <?php if (get_sub_field('catalogue_text') != "") : ?>
                                                                <p><?php echo get_sub_field('catalogue_text'); ?></p>
                                                            <?php endif; ?>
                                                            <?php /*
                                                            $link = get_sub_field('catalogue_download_link');
                                                            if ($link) :
                                                                $link_url = $link['url'];
                                                                $link_title = $link['title'];
                                                                $link_target = $link['target'] ? $link['target'] : '_self';
                                                            ?>
                                                                <a href="<?php echo esc_url($link_url); ?>" class="secondary-button" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                                                            <?php endif; */ ?>
                                                            <span class="download_pdf_button primary-button">Download</span>
                                                            <?php if (get_sub_field('pdf_link') != "") : ?>
                                                                <input type="hidden" class="pdf-data" name="pdf-link" value="<?php echo get_sub_field('pdf_link'); ?>" />
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <?php if (have_rows('customization')) : ?>
                                <?php while (have_rows('customization')) : the_row(); ?>
                                    <?php if (get_sub_field('heading') != "") : ?>
                                        <div class="customization-content">
                                            <h5><?php echo get_sub_field('heading'); ?></h5>
                                            <p><?php echo get_sub_field('text'); ?></p>
                                        </div>
                                        <div class="customization">
                                            <?php if (have_rows('items')) : ?>
                                                <?php while (have_rows('items')) : the_row(); ?>
                                                    <div class="items">
                                                        <?php if (get_sub_field('image') != "") : ?>
                                                            <div class="fc">
                                                                <img src="<?php echo get_sub_field('image'); ?>" class="" alt="">
                                                            </div>
                                                        <?php endif; ?>
                                                        <div>
                                                            <h5><?php echo get_sub_field('heading'); ?></h5>
                                                            <p><?php echo get_sub_field('text'); ?></p>
                                                        </div>
                                                    </div>
                                                <?php endwhile; ?>
                                            <?php endif; ?>
                                        </div>
                                    <?php endif; ?>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>


<?php if (have_rows('testimonials', 'option')) : ?>
    <?php while (have_rows('testimonials', 'option')) : the_row(); ?>
        <section class="mt product-page-testimonials">
            <div class="container">
                <div class="row text-center">
                    <div class="col-12">
                        <h4><?php echo get_sub_field('heading'); ?></h4>
                    </div>
                </div>
                <?php if (have_rows('items')) : ?>
                    <div class="row">
                        <div class="col-12">
                            <div class="pp-testimonial-slider">
                                <?php while (have_rows('items')) : the_row(); ?>
                                    <div class="items p-rel">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/quotes.svg" class="img-fluid" alt="">
                                        <h5><?php echo get_sub_field('heading'); ?></h5>
                                        <p><?php echo get_sub_field('content'); ?></p>
                                        <h6><?php echo get_sub_field('name'); ?></h6>
                                        <span><?php echo get_sub_field('location'); ?></span>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<section class="mt product-page d-md-none d-block">
    <div class="container">
        <div class="row">
            <div class="col-12 sb">
                <?php dynamic_sidebar('product-sidebar'); ?>
                <?php if (have_rows('free_quote')) : ?>
                    <?php while (have_rows('free_quote')) : the_row(); ?>
                        <?php if (get_sub_field('catalogue_heading') != "") : ?>
                            <div class="col-fq p-rel">
                                <div class="">
                                    <h5><?php echo get_sub_field('catalogue_heading'); ?></h5>
                                    <p><?php echo get_sub_field('catalogue_text'); ?></p>
                                    <?php
                                    $link = get_sub_field('catalogue_download_link');
                                    if ($link) :
                                        $link_url = $link['url'];
                                        $link_title = $link['title'];
                                        $link_target = $link['target'] ? $link['target'] : '_self';
                                    ?>
                                        <a href="<?php echo esc_url($link_url); ?>" class="primary-button" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="popup-form-2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Download Your Free PDF <img src="https://uxsingh.com/revacranes/wp-content/uploads/2024/02/icons8-download-48.png" width="24"></h4>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo do_shortcode('[gravityform id="3" title="true" ajax="true"]'); ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>