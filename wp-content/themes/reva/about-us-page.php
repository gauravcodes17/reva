<?php

/***
 * Template Name: About Us Page Template
 */
get_header();
?>

<?php if (have_rows('banner_section')) : ?>
    <?php while (have_rows('banner_section')) : the_row(); ?>
        <div class="main-banner" style="background-image: url(' <?php echo get_sub_field("background_image"); ?>');">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2><?php echo get_sub_field('heading'); ?></h2>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('about_industries_section')) : ?>
    <?php while (have_rows('about_industries_section')) : the_row(); ?>
        <section class="mt about-us-industries-section">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-12 d-md-block d-none">
                        <?php if (have_rows('images_slider')) : ?>
                            <div class="about-us-awards-slider">
                                <?php while (have_rows('images_slider')) : the_row(); ?>
                                    <div class="items">
                                        <img src="<?php echo get_sub_field('image'); ?>" class="img-fluid w-100" alt="">
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="col-lg-6 col-12">
                        <h5><?php echo get_sub_field('sub_heading'); ?></h5>
                        <h3><?php echo get_sub_field('heading'); ?></h3>
                        <?php if (have_rows('images_slider')) : ?>
                            <div class="about-us-awards-slider d-md-none d-block as-slider">
                                <?php while (have_rows('images_slider')) : the_row(); ?>
                                    <div class="items">
                                        <img src="<?php echo get_sub_field('image'); ?>" class="img-fluid w-100" alt="">
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        <?php endif; ?>
                        <p class="tagline"><?php echo get_sub_field('tagline'); ?></p>
                        <p><?php echo get_sub_field('content'); ?></p>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('mission_vision_section')) : ?>
    <?php while (have_rows('mission_vision_section')) : the_row(); ?>
        <section class="mt mv-section p-rel" style="background-image: url(' <?php echo get_sub_field("background_image"); ?>');">
            <div class="bg-opacity"></div>
            <div class="container z-1">
                <div class="row">
                    <div class="col-md-6">
                        <h4><?php echo get_sub_field('mission_heading'); ?></h4>
                        <p><?php echo get_sub_field('mission_content'); ?></p>
                    </div>
                    <div class="col-md-6">
                        <h4><?php echo get_sub_field('vision_heading'); ?></h4>
                        <p><?php echo get_sub_field('vision_content'); ?></p>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('journey_section')) : ?>
    <?php while (have_rows('journey_section')) : the_row(); ?>
        <section class="mt journey-section">
            <div class="container">
                <div class="row">
                    <div class="col-12 d-flex">
                        <span class="line"></span>
                        <h3><?php echo get_sub_field('heading'); ?></h3>
                    </div>
                </div>
                <?php if (have_rows('journey')) : ?>
                    <div class="row">
                        <?php while (have_rows('journey')) : the_row(); ?>
                            <div class="col-lg-4 col-md-6 col-12">
                                <div class="journey-items p-rel">
                                    <h4><?php echo get_sub_field('title'); ?></h4>
                                    <span><?php echo get_sub_field('label'); ?></span>
                                    <p><?php echo get_sub_field('description'); ?></p>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>
                <div class="about-us-catalogue">
                    <div class="row align-items-center justify-content-between">
                        <div class="col-lg-8 col-md-8 col-12">
                            <h6><?php echo get_sub_field('catalogue_heading'); ?></h6>
                        </div>
                        <div class="col-lg-3 col-md-4 col-12 text-right">
                            <?php
                            $link = get_sub_field('catalogue');
                            if ($link) :
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                                <a href="<?php echo esc_url($link_url); ?>" class="secondary-button" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('timeline_journey')) : ?>
    <?php while (have_rows('timeline_journey')) : the_row(); ?>
        <section class="mt o-hidden" id="timeline__section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <span class="line"></span>
                        <h3><?php echo get_sub_field('heading'); ?></h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="table_wrapper">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/start.svg" alt="" class="start__image">
                    <?php if (have_rows('timeline')) : ?>
                        <div class="timeline">
                            <?php while (have_rows('timeline')) : the_row(); ?>
                                <div class="checkpoint">
                                    <div>
                                        <h2><?php echo get_sub_field('year'); ?></h2>
                                        <p><?php echo get_sub_field('content'); ?></p>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                    <p class="end__content"><?php echo get_sub_field('end_content'); ?></p>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/end-image.svg" alt="" class="end__image">
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('our_team')) : ?>
    <?php while (have_rows('our_team')) : the_row(); ?>
        <section class="mt team-section">
            <div class="container">
                <div class="row">
                    <div class="col-12 d-flex">
                        <span class="line"></span>
                        <h3><?php echo get_sub_field('heading'); ?></h3>
                    </div>
                </div>
                <?php if (have_rows('team')) : ?>
                    <?php while (have_rows('team')) : the_row(); ?>
                        <div class="row align-items-center">
                            <div class="col-lg-3 col-md-4 col-12">
                                <img src="<?php echo get_sub_field('image'); ?>" class="img-fluid w-100" alt="">
                            </div>
                            <div class="col-lg-9 col-md-8 col-12">
                                <p><?php echo get_sub_field('content'); ?></p>
                                <h5><?php echo get_sub_field('name'); ?></h5>
                                <h6><?php echo get_sub_field('designation'); ?></h6>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
                <?php if (have_rows('video_section')) : ?>
                    <?php while (have_rows('video_section')) : the_row(); ?>
                        <div class="vdo-section">
                            <div class="row align-items-center p-rel">
                                <div class="col-md-8 col-12">
                                    <h4><?php echo get_sub_field('tagline'); ?></h4>
                                    <a href="#vdo2" class="award-video">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/au-play-icon.svg" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="col-md-4 col-12">
                                    <img src="<?php echo get_sub_field('image'); ?>" class="img-fluid" alt="">
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
                <?php if (have_rows('video_section')) : ?>
                    <?php while (have_rows('video_section')) : the_row(); ?>
                        <div class="mfp-hide" id="vdo2" style="max-width: 100%;margin: 0 auto;display: flex;justify-content: center;">
                            <iframe width="560" height="315" src="<?php echo get_sub_field('video_link'); ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                            <button title="Close (Esc)" type="button" class="mfp-close">×</button>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('patents_section')) : ?>
    <?php while (have_rows('patents_section')) : the_row(); ?>
        <section class="mt patents-section">
            <div class="container">
                <div class="row">
                    <div class="col-12 d-flex">
                        <span class="line"></span>
                        <h3><?php echo get_sub_field('heading'); ?></h3>
                    </div>
                </div>
                <?php if (have_rows('pdfs_lists')) : ?>
                    <div class="row">
                        <div class="col-12">
                            <div class="patents-slider">
                                <?php while (have_rows('pdfs_lists')) : the_row(); ?>
                                    <div class="items">
                                        <?php
                                        $link = get_sub_field('download_link');
                                        if ($link) :
                                            $link_url = $link['url'];
                                            $link_title = $link['title'];
                                            $link_target = $link['target'] ? $link['target'] : '_self';
                                        ?>
                                            <a href="<?php echo esc_url($link_url); ?>" class="tertiary-button" target="<?php echo esc_attr($link_target); ?>">
                                                <img src="<?php echo get_sub_field('image'); ?>" class="img-fluid w-100" alt="">
                                            </a>
                                        <?php endif; ?>
                                        <?php
                                        $link = get_sub_field('download_link');
                                        if ($link) :
                                            $link_url = $link['url'];
                                            $link_title = $link['title'];
                                            $link_target = $link['target'] ? $link['target'] : '_self';
                                        ?>
                                            <div class="text-center">
                                                <a href="<?php echo esc_url($link_url); ?>" class="tertiary-button" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('certifications')) : ?>
    <?php while (have_rows('certifications')) : the_row(); ?>
        <section class="mt certificate-section">
            <div class="container">
                <div class="row">
                    <div class="col-12 d-flex">
                        <span class="line"></span>
                        <h3><?php echo get_sub_field('heading'); ?></h3>
                    </div>
                </div>
                <?php if (have_rows('certificate')) : ?>
                    <div class="row">
                        <div class="col-12">
                            <div class="certificate-slider">
                                <?php while (have_rows('certificate')) : the_row(); ?>
                                    <div class="items">
                                        <a href="<?php echo get_sub_field('pdf_link'); ?>" target="_blank">
                                            <img src="<?php echo get_sub_field('image'); ?>" class="img-fluid w-100" alt="">
                                        </a>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('infrastructure')) : ?>
    <?php while (have_rows('infrastructure')) : the_row(); ?>
        <section class="mt infrastructure-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-12">
                        <span class="line"></span>
                        <h3><?php echo get_sub_field('heading'); ?></h3>
                    </div>
                    <div class="col-md-6 col-12">
                        <p><?php echo get_sub_field('content'); ?></p>
                    </div>
                </div>
                <?php if (have_rows('gallery_section')) : ?>
                    <?php while (have_rows('gallery_section')) : the_row(); ?>
                        <div class="row">
                            <div class="col-12">
                                <div class="about-gallery-section">
                                    <?php if (have_rows('images')) : ?>
                                        <?php while (have_rows('images')) : the_row(); ?>
                                            <div>
                                                <img src="<?php echo get_sub_field('image'); ?>" class="img-fluid w-100" alt="">
                                            </div>
                                        <?php endwhile; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
                <div class="about-us-catalogue">
                    <div class="row align-items-center justify-content-between">
                        <div class="col-lg-8 col-md-8 col-12">
                            <h6><?php echo get_sub_field('infra_catalogue_heading'); ?></h6>
                        </div>
                        <div class="col-lg-3 col-md-4 col-12 text-right">
                            <?php
                            $link = get_sub_field('infra_catalogue_download_link');
                            if ($link) :
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                                <a href="<?php echo esc_url($link_url); ?>" class="secondary-button" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="about-us-catalogue">
                    <div class="row align-items-center justify-content-between">
                        <div class="col-lg-8 col-md-8 col-12">
                            <h6><?php echo get_sub_field('infra_catalogue_heading_two'); ?></h6>
                        </div>
                        <div class="col-lg-3 col-md-4 col-12 text-right">
                            <?php
                            $link = get_sub_field('infra_catalogue_download_link_two');
                            if ($link) :
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                                <a href="<?php echo esc_url($link_url); ?>" class="secondary-button" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>