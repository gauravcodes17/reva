<?php
setPostViews(get_the_ID());
get_header();
?>

<?php /*if (have_rows('banner_section')) : ?>
    <?php while (have_rows('banner_section')) : the_row(); ?>
        <div class="main-banner" style="background-image: url(' <?php echo get_sub_field("background_image"); ?>');">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2><?php echo get_sub_field('heading'); ?></h2>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; */ ?>


<div class="main-banner" style="background-image: url(' <?php echo get_field('blog_banner', 'option'); ?>');">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2><?php echo get_the_title(); ?></h2>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="popup-form-1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Download Your Free E-Book <img src="https://uxsingh.com/revacranes/wp-content/uploads/2024/02/icons8-download-48.png" width="24"></h4>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo do_shortcode('[gravityform id="3" title="true" ajax="true"]'); ?>
            </div>
        </div>
    </div>
</div>

<section class="mt blog-detailed sdb">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-12">
                <div class="blog-detailed-wrapper">
                    <h3><?php echo get_the_title(); ?></h3>
                    <span class="blog-date"></span><?php echo get_the_date('F d, Y'); ?></span>
                    <div>
                        <?php echo get_the_content(); ?>
                        <?php if (get_field('link') != "") : ?>
                            <input type="hidden" class="pdf-data" name="pdf-link" value="<?php echo get_field('link'); ?>" />
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-12">
                <div class="blog-right-section">
                    <div class="categories">
                        <?php dynamic_sidebar('blog-categories-sidebar'); ?>
                    </div>
                    <div class="recent-post">
                        <h4>Inspiration</h4>
                        <?php //dynamic_sidebar('recent-post-sidebar'); 
                        ?>
                        <?php get_template_part('template-parts/inspiration-sidebar'); ?>
                    </div>
                    <div class="recent-post">
                        <h4>Recent News</h4>
                        <?php //dynamic_sidebar('recent-post-sidebar'); 
                        ?>
                        <?php get_template_part('template-parts/recent-post-sidebar'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>