<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta <?php bloginfo('charset'); ?>>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/reva-favicon.png" type="image/png">
    <title>Reva || <?php echo get_the_title(); ?></title>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <div class="header p-rel">
        <div class="top-header">
            <div class="top-tagline"><?php echo get_field('header_tagline', 'option'); ?></div>
            <div class="top-details">
                <div class="top-contact-details">
                    <p>Call us: <a href="tel:<?php echo get_field('call_us', 'option'); ?>"><?php echo get_field('call_us', 'option'); ?></a></p>
                    <p>Mail us: <a href="mailto:<?php echo get_field('mail_us', 'option'); ?>"><?php echo get_field('mail_us', 'option'); ?></a></p>
                </div>
                <div class="top-social-icons">
                    <a href="<?php echo get_field('linkedin_link', 'option'); ?>" target="_blank">
                        <img src="<?php echo get_field('linkedin', 'option'); ?>" class="img-fluid" alt="">
                    </a>
                    <a href="<?php echo get_field('youtube_link', 'option'); ?>" target="_blank">
                        <img src="<?php echo get_field('youtube', 'option'); ?>" class="img-fluid" alt="">
                    </a>
                    <a href="<?php echo get_field('facebook_link', 'option'); ?>" target="_blank">
                        <img src="<?php echo get_field('facebook', 'option'); ?>" class="img-fluid" alt="">
                    </a>
                    <a href="<?php echo get_field('instagram_link', 'option'); ?>" target="_blank">
                        <img src="<?php echo get_field('instagram', 'option'); ?>" class="img-fluid" alt="">
                    </a>
                </div>
            </div>
        </div>
        <div class="main-header d-header">
            <div class="main-logo">
                <img src="<?php echo get_field('header_logo', 'option'); ?>" class="img-fluid" alt="">
            </div>
            <div class="header-menu-links">
                <?php
                // Header Menu
                if (function_exists('register_primary_menu')) :
                    wp_nav_menu([
                        'theme_location' => 'Header',
                        'menu_class' => 'hv',
                    ]);
                endif;
                ?>
                <?php if (get_field('pdf_link', 'option') != "") : ?>
                    <input type="hidden" class="pdf-data" name="pdf-link" value="<?php echo get_field('pdf_link', 'option'); ?>" />
                <?php endif; ?>
            </div>
            <div class="search-contact">
                <div class="header-search">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/search.svg" class="img-fluid" alt="">
                </div>
                <!-- <button>Contact Us</button> -->
                <?php
                $link = get_field('contact_button', 'option');
                if ($link) :
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                ?>
                    <a href="<?php echo esc_url($link_url); ?>" class="tertiary-button" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                <?php endif; ?>
            </div>
        </div>
        <div class="main-header m-header">
            <div class="main-logo">
                <img src="<?php echo get_field('header_logo', 'option'); ?>" class="img-fluid" alt="">
            </div>
            <div class="mobile-menu-ws">
                <div class="search-contact">
                    <div class="header-search">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/m-search.svg" class="img-fluid" alt="">
                    </div>
                </div>
                <div class="header-menu-links">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/hamburger.svg" class="img-fluid" alt="" id="m-menu-show">
                    <div class="mobile-menu-r">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/cross.svg" class="img-fluid" alt="" id="m-menu-show">
                        <?php
                        // Header Menu
                        if (function_exists('register_primary_menu')) :
                            wp_nav_menu([
                                'theme_location' => 'Header',
                                'menu_class' => 'hv',
                            ]);
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="search-bar" id="search-bar">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/search.svg" class="img-fluid" alt="">
            <!-- <input type="text" placeholder="Search"> -->
            <?php echo do_shortcode('[ivory-search id="930" title="Default Search Form"]'); ?>
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/cross-black.svg" class="img-fluid close-search-bar" alt="">
        </div>
    </div>