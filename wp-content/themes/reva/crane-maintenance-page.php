<?php

/***
 * Template Name: Crane Maintenance Page Template
 */
get_header();
?>

<?php if (have_rows('banner_section')) : ?>
    <?php while (have_rows('banner_section')) : the_row(); ?>
        <div class="main-banner" style="background-image: url(' <?php echo get_sub_field("background_image"); ?>');">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2><?php echo get_sub_field('heading'); ?></h2>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('crane_maintenance_videos')) : ?>
    <?php while (have_rows('crane_maintenance_videos')) : the_row(); ?>
        <section class="mt crane-maintenance">
            <div class="container">
                <div class="row">
                    <?php $i = 1; ?>
                    <?php if (have_rows('videos')) : ?>
                        <?php while (have_rows('videos')) : the_row(); ?>
                            <div class="col-lg-4 col-md-6 col-12 <?php if ($i > 9) : echo 'hideVideos';
                                                                    else : echo 'nhv';
                                                                    endif; ?>">
                                <div class="items">
                                    <div class="p-rel">
                                        <img src="<?php echo get_sub_field('thumbnail'); ?>" class="img-fluid w-100" alt="">
                                        <a href="#t-story<?php echo $i; ?>" class="award-video" href="<?php echo get_sub_field('video_link_text'); ?>">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/youtube-icon.png" class="img-fluid yi" alt="">
                                        </a>
                                    </div>
                                    <h4><?php echo get_sub_field('title'); ?></h4>
                                    <p><?php echo get_sub_field('troubleshooting'); ?></p>
                                    <p><?php echo get_sub_field('solution'); ?></p>
                                    <a href="#t-story<?php echo $i; ?>" class="award-video wn" href="<?php echo get_sub_field('video_link_text'); ?>">
                                        <?php echo get_sub_field('video_link_text'); ?>
                                    </a>
                                </div>
                            </div>
                            <?php $i++; ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
                <?php if ($i > 9) : ?>
                    <div class="row text-center mt-3">
                        <div class="col-12">
                            <button class="see-more-cm secondary-button">
                                See More
                            </button>
                        </div>
                    </div>
                <?php endif; ?>
                <?php $k = 1; ?>
                <?php while (have_rows('videos')) : the_row(); ?>
                    <div class="mfp-hide" id="t-story<?php echo $k; ?>" style="max-width: 100%;margin: 0 auto;display: flex;justify-content: center;">
                        <iframe width="560" height="315" src="<?php echo get_sub_field('video_link'); ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                        <button title="Close (Esc)" type="button" class="mfp-close">×</button>
                    </div>
                    <?php $k++; ?>
                <?php endwhile; ?>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>