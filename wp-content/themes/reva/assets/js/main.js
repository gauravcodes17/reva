(function ($) {
  $(document).ready(function () {
    $(window).scroll(function () {
      if ($(window).scrollTop() > 0) {
        // console.log("scrollllll");
        $(".header .main-header").addClass("add-sticky-header");
      } else {
        $(".header .main-header").removeClass("add-sticky-header");
      }
    });

    $(".header-search").click(function () {
      // console.log("search-bar");
      // $("#search-bar").show(100);
      $("#search-bar").addClass("show-search-bar");
    });

    $(".close-search-bar").click(function () {
      // console.log("search-bar");
      // $("#search-bar").hide(100);
      $("#search-bar").removeClass("show-search-bar");
    });

    $(".see-more-cm").click(function () {
      if ($(".see-more-cm").hasClass("sl") == true) {
        console.log("remove");
        $(".crane-maintenance .row .col-lg-4").addClass("active-next");
        $(this).removeClass("sl");
        $(this).text("See Less");
      } else {
        $(".crane-maintenance .row .col-lg-4").removeClass("active-next");
        $(this).addClass("sl");
        $(this).text("See More");
      }
    });

    //download whitepaper form -- for download link put in the form
    // $(".download_pdf_button").click(function () {
    //   let link = $(".pdf-data").attr("value");
    //   // let source = $(this).attr("data-source");
    //   $("#paper-link").val(link);
    //   // $("#custom-source").val(source);
    // });

    $(".download_pdf_button").click(function () {
      // console.log("hello");
      let link = $(".pdf-data").attr("value");
        $("#input_3_5").val(link);

      $("#popup-form-1").modal("show");
      $("#popup-form-2").modal("show");
    });

    $("#popup-form-1 .close").click(function () {
      // console.log("hello");
      $("#popup-form-1").modal("hide");
      $("#popup-form-2").modal("hide");
    });


    $(".download_pdf_button_header").click(function () {
      // console.log("hello");
      let link = $(".pdf-data").attr("value");
      $("#input_3_5").val(link);

      $("#popup-form-3").modal("show");
    });

    $("#popup-form-3 .close").click(function () {
      // console.log("hello");
      $("#popup-form-3").modal("hide");
    });

    /* header toggle */

    $("#m-menu-show").click(function() {
      $(
        ".header .main-header.m-header .mobile-menu-ws .mobile-menu-r"
      ).show(200);
    });

    $(".header .main-header.m-header .mobile-menu-ws .mobile-menu-r img").click(
      function () {
        $(".header .main-header.m-header .mobile-menu-ws .mobile-menu-r").hide(
          200
        );
      }
    );

    // $(".header .main-header.m-header .header-menu-links ul.hv > li a").click(function() {
    //   $(this).siblings("ul.sub-menu").toggle();
    // });

    $(".about-us-slider").slick({
      autoplay: true,
      autoplaySpeed: 4000,
      infinite: true,
      dots: false,
      margin: 20,
      slidesToShow: 8,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 6,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 6,
            slidesToScroll: 1,
            dots: true,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
          },
        },
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ],
    });

    $(".case-study-slider").slick({
      autoplay: true,
      autoplaySpeed: 5000,
      infinite: true,
      dots: false,
      margin: 20,
      slidesToShow: 4,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ],
    });

    $(".industry-slider").slick({
      autoplay: true,
      autoplaySpeed: 8000,
      infinite: true,
      dots: true,
      margin: 50,
      slidesToShow: 2,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ],
    });

    $(".awards-slider").slick({
      autoplay: true,
      autoplaySpeed: 4000,
      infinite: true,
      dots: false,
      margin: 20,
      centerMode: true,
      centerPadding: "60px",
      slidesToShow: 4,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 1099,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: true,
          },
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: true,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: true,
            centerMode: false,
            centerPadding: "0",
          },
        },
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ],
    });

    $(".blog-slider").slick({
      autoplay: true,
      autoplaySpeed: 5000,
      infinite: true,
      dots: false,
      margin: 20,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: true,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            centerMode: true,
            centerPadding: "120px",
          },
        },
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ],
    });

    $(".testimonial-slider").slick({
      autoplay: true,
      autoplaySpeed: 3000,
      infinite: true,
      dots: true,
      margin: 20,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: true,
            centerPadding: "40px",
          },
        },
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ],
    });

    $(".about-us-awards-slider").slick({
      autoplay: true,
      autoplaySpeed: 5000,
      infinite: true,
      dots: true,
      margin: 20,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
          },
        },
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ],
    });

    $(".patents-slider").slick({
      autoplay: true,
      autoplaySpeed: 5000,
      infinite: true,
      dots: false,
      margin: 20,
      slidesToShow: 4,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: true,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: true,
          },
        },
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ],
    });

    $(".certificate-slider").slick({
      autoplay: true,
      autoplaySpeed: 5000,
      infinite: true,
      dots: false,
      margin: 20,
      slidesToShow: 4,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: true,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: true,
          },
        },
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ],
    });

    $(".cp-testimonial-slider").slick({
      autoplay: true,
      autoplaySpeed: 5000,
      infinite: true,
      dots: true,
      margin: 20,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: true,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            centerMode: true,
            centerPadding: "60px",
          },
        },
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ],
    });

    $(".gallery-slider").slick({
      autoplay: true,
      autoplaySpeed: 0,
      speed: 8000,
      infinite: true,
      dots: false,
      margin: 20,
      slidesToShow: 4,
      slidesToScroll: 1,
      arrows: false,
      cssEase: "linear",
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: true,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: true,
          },
        },
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ],
    });

    $(".training-and-workdhops-slider").slick({
      autoplay: true,
      autoplaySpeed: 5000,
      // speed: 5000,
      infinite: true,
      dots: false,
      margin: 20,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: true,
            centerPadding: "50px",
          },
        },
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ],
    });

    $(".training-video-feedbacks-slider").slick({
      autoplay: true,
      autoplaySpeed: 5000,
      // speed: 5000,
      infinite: true,
      dots: false,
      margin: 20,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: true,
            centerPadding: "120px",
          },
        },
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ],
    });

    $(".pp-testimonial-slider").slick({
      autoplay: true,
      autoplaySpeed: 8000,
      infinite: true,
      dots: true,
      margin: 20,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: true,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: true,
            centerPadding: "80px",
            dots: true,
          },
        },
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ],
    });

    $(".award-video").each(function () {
      console.log("hello");
      $(this).magnificPopup({
        type: "inline",
        midClick: true,
      });
    });
  });
})(jQuery);
