<?php

/***
 * Template Name: Career Page Template
 */
get_header();
?>

<?php if (have_rows('banner_section')) : ?>
    <?php while (have_rows('banner_section')) : the_row(); ?>
        <div class="main-banner" style="background-image: url(' <?php echo get_sub_field("background_image"); ?>');">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2><?php echo get_sub_field('heading'); ?></h2>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('career_page_content')) : ?>
    <?php while (have_rows('career_page_content')) : the_row(); ?>
        <section class="mt career-page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-12">
                        <h5><?php echo get_sub_field('sub_heading'); ?></h5>
                        <h3><?php echo get_sub_field('heading'); ?></h3>
                        <h6><?php echo get_sub_field('sub_heading'); ?></h6>
                    </div>
                    <div class="col-md-6 col-12">
                        <p class="bold"><?php echo get_sub_field('content_1'); ?></p>
                        <p><?php echo get_sub_field('content_2'); ?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="career-form">
                            <?php echo do_shortcode(get_sub_field('form_shortcode')); ?>
                        </div>
                    </div>
                </div>
                <div class="career-catalogue">
                    <div class="row justify-content-between align-items-center">
                        <div class="col-lg-8 col-md-8 col-12">
                            <h4><?php echo get_sub_field('download_profile_text'); ?></h4>
                        </div>
                        <div class="col-lg-3 col-md-4 col-12 text-right">
                            <?php
                            $link = get_sub_field('download_profile_link');
                            if ($link) :
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                                <a href="<?php echo esc_url($link_url); ?>" class="secondary-button" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('career_page_testimonials')) : ?>
    <?php while (have_rows('career_page_testimonials')) : the_row(); ?>
        <section class="mt cp-testimonials">
            <div class="container">
                <div class="row text-center">
                    <div class="col-12">
                        <h3><?php echo get_sub_field('heading'); ?></h3>
                    </div>
                </div>
                <?php if (have_rows('testimonials')) : ?>
                    <div class="row">
                        <div class="col-12">
                            <div class="cp-testimonial-slider">
                                <?php while (have_rows('testimonials')) : the_row(); ?>
                                    <div class="items">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/quotes.svg" class="img-fluid" alt="">
                                        <p><?php echo get_sub_field('content'); ?></p>
                                        <h5><?php echo get_sub_field('name'); ?></h5>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('gallery_slider')) : ?>
    <section class="mt gallery-slider-section">
        <div class="container-fluid gx-0">
            <div class="row gx-0">
                <div class="col-12">
                    <div class="gallery-slider">
                        <?php while (have_rows('gallery_slider')) : the_row(); ?>
                            <div class="items">
                                <img src="<?php echo get_sub_field('image'); ?>" class="img-fluid w-100" alt="">
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php get_footer(); ?>