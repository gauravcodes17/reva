<?php

get_header();
?>

<?php if (have_rows('banner_section')) : ?>
    <?php while (have_rows('banner_section')) : the_row(); ?>
        <div class="main-banner" style="background-image: url(' <?php echo get_sub_field("background_image"); ?>');">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2><?php echo get_sub_field('heading'); ?></h2>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('industries_page_content')) : ?>
    <?php while (have_rows('industries_page_content')) : the_row(); ?>
        <section class="mt industries-content">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <?php if (get_sub_field('content') != "") : ?>
                            <p><?php echo get_sub_field('content'); ?></p>
                        <?php endif; ?>
                        <?php if (get_sub_field('full_image') != "") : ?>
                            <div class="fah-img">
                                <img src="<?php echo get_sub_field('full_image') ?>" class="img-fluid w-100" alt="">
                            </div>
                        <?php endif; ?>
                        <?php if (have_rows('half_image')) : ?>
                            <?php while (have_rows('half_image')) : the_row(); ?>
                                <?php if (get_sub_field('image_one') != "") : ?>
                                    <div class="fah-img">
                                        <div class="row">
                                            <div class="col-md-6 col-12 p-rel">
                                                <?php if (get_sub_field('image_one') != "") : ?>
                                                    <img src="<?php echo get_sub_field('image_one') ?>" class="img-fluid w-100" alt="">
                                                    <?php if (get_sub_field('image_text1') != "") : ?>
                                                        <p class="text-oi"><?php echo get_sub_field('image_text1') ?></p>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </div>
                                            <div class="col-md-6 col-12 p-rel">
                                                <?php if (get_sub_field('image_two') != "") : ?>
                                                    <img src="<?php echo get_sub_field('image_two') ?>" class="img-fluid w-100" alt="">
                                                    <?php if (get_sub_field('image_text2') != "") : ?>
                                                        <p class="text-oi"><?php echo get_sub_field('image_text2') ?></p>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endwhile; ?>
                        <?php endif; ?>
                        <?php if (get_sub_field('text') != "") : ?>
                            <div class="content-after-img">
                                <p><?php echo get_sub_field('text'); ?></p>
                            </div>
                        <?php endif; ?>
                        <?php if (have_rows('tabular_content')) : ?>
                            <?php while (have_rows('tabular_content')) : the_row(); ?>
                                <?php if (get_sub_field('heading') != "") : ?>
                                    <h5><?php echo get_sub_field('heading'); ?></h5>
                                <?php endif; ?>
                                <?php if (have_rows('table')) : ?>
                                    <?php while (have_rows('table')) : the_row(); ?>
                                        <div class="i-table">
                                            <table>
                                                <thead>
                                                    <?php if (have_rows('table_text')) : ?>
                                                        <?php $i = 1; ?>
                                                        <?php while (have_rows('table_text')) : the_row(); ?>
                                                            <tr>
                                                                <th><?php echo get_sub_field('text'); ?></th>
                                                                <th><?php echo get_sub_field('text_2'); ?></th>
                                                                <th><?php echo get_sub_field('text_3'); ?></th>
                                                            </tr>
                                                            <?php if ($i == 1) : break;
                                                            endif; ?>
                                                            <?php $i++; ?>
                                                        <?php endwhile; ?>
                                                    <?php endif; ?>
                                                </thead>
                                                <tbody>
                                                    <?php if (have_rows('table_text')) : ?>
                                                        <?php $j = 1; ?>
                                                        <?php while (have_rows('table_text')) : the_row(); ?>
                                                            <tr>
                                                                <td><?php echo get_sub_field('text'); ?></td>
                                                                <td><?php echo get_sub_field('text_2'); ?></td>
                                                                <td><?php echo get_sub_field('text_3'); ?></td>
                                                            </tr>
                                                            <?php $j++; ?>
                                                        <?php endwhile; ?>
                                                    <?php endif; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php endwhile; ?>
                                <?php endif; ?>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>