<?php

get_header();

$category = get_queried_object();
$cat_id = $category->term_id;
$cat_name = $category->name;
?>

<?php /* if (have_rows('banner_section')) : ?>
    <?php while (have_rows('banner_section')) : the_row(); */ ?>
<div class="main-banner" style="background-image: url(' <?php echo get_template_directory_uri(); ?>/assets/images/about-us-main-banner.png');">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Blogs</h2>
            </div>
        </div>
    </div>
</div>
<?php /* endwhile; ?>
<?php endif; */ ?>

<div class="blog-posts blogs-section mt">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-12">
                <div class="" id="blog-search-form-container">
                    <h1><?php echo $cat_name ?></h1>
                    <?php
                    $paged = get_query_var('paged') ? get_query_var('paged') : 1;
                    $args = array(
                        'paged' => $paged,
                        'post_type'   => 'post',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'category', //double check your taxonomy name in you dd 
                                'field'    => 'id',
                                'terms'    => $cat_id,
                            ),
                        ),
                        'posts_per_page' => 10,
                        'post_status' => 'publish',
                        'order'    => 'DESC',
                        'orderby'    => 'ID',
                    );
                    $query = new WP_Query($args);

                    if ($query->have_posts()) :
                    ?>
                        <div class="row mt-4">
                            <?php while ($query->have_posts()) : $query->the_post(); ?>
                                <?php $cats = get_the_category(get_the_ID()); ?>
                                <div class="col-lg-6 col-12">
                                    <div class="blog-wrap">
                                        <div class="blog-content">
                                            <div class="mmb d-flex justify-content-between">
                                                <span><img src="<?php echo get_template_directory_uri(); ?>/assets/images/calender.svg" class="img-fluid" alt="date"><?php echo get_the_date('F d, Y'); ?></span>
                                                <span class="category-name"><?php echo $cats[0]->name; ?></span>
                                            </div>
                                            <a href="<?php echo get_the_permalink(); ?>">
                                                <h5><?php echo get_the_title(); ?></h5>
                                            </a>
                                            <p><?php echo get_field('excerpt_content'); ?></p>
                                            <a href="<?php echo get_the_permalink(); ?>">Read More >></a>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                    <div class="blog-pagenavi">
                        <div class="row text-center">
                            <div class="col-md-12">
                                <div class="prev-next">
                                    <?php wp_pagenavi(array('query' => $query)); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-12">
                <div class="blog-right-section">
                    <div class="categories">
                        <?php dynamic_sidebar('blog-categories-sidebar'); ?>
                    </div>
                    <div class="recent-post">
                        <h4>Inspiration</h4>
                        <?php //dynamic_sidebar('recent-post-sidebar'); 
                        ?>
                        <?php get_template_part('template-parts/inspiration-sidebar'); ?>
                    </div>
                    <div class="recent-post">
                        <h4>Recent News</h4>
                        <?php //dynamic_sidebar('recent-post-sidebar'); 
                        ?>
                        <?php get_template_part('template-parts/recent-post-sidebar'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <?php get_footer(); ?>