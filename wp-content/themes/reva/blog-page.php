<?php

/***
 * Template Name: Blog Page Template
 */
get_header();
?>

<?php if (have_rows('banner_section')) : ?>
    <?php while (have_rows('banner_section')) : the_row(); ?>
        <div class="main-banner" style="background-image: url(' <?php echo get_sub_field("background_image"); ?>');">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2><?php echo get_sub_field('heading'); ?></h2>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>

<section class="mt blog-posts">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <?php echo do_shortcode('[searchandfilter id="975"]') ?>
            </div>
        </div>
    </div>
    <div class="container" id="blog-search-form-container">
        <?php
        $paged = get_query_var('paged') ? get_query_var('paged') : 1;
        $args = array(
            'paged' => $paged,
            'post_type'   => 'post',
            'posts_per_page' => 18,
            'post_status' => 'publish',
            'order'    => 'DESC',
            'orderby'    => 'ID',
            'search_filter_id' => 975,
        );
        $query = new WP_Query($args);

        if ($query->have_posts()) :
        ?>
            <div class="row mt-4">
                <?php while ($query->have_posts()) : $query->the_post(); ?>
                    <?php $cats = get_the_category(get_the_ID()); ?>
                    <div class="col-lg-4 col-md-6 col-12">
                        <div class="blog-wrap">
                            <div class="blog-content">
                                <div class="mmb d-flex justify-content-between">
                                    <!-- <span><img src="<?php // echo get_template_directory_uri(); ?>/assets/images/calender.svg" class="img-fluid" alt="date"><?php // echo get_the_date('F d, Y'); ?></span> -->
                                    <span class="category-name"><?php echo $cats[0]->name; ?></span>
                                </div>
                                <a href="<?php echo get_the_permalink(); ?>">
                                    <h5><?php echo get_the_title(); ?></h5>
                                </a>
                                <p><?php echo get_field('excerpt_content'); ?></p>
                                <a href="<?php echo get_the_permalink(); ?>">Read More >></a>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
        <div class="blog-pagenavi">
            <div class="row text-center">
                <div class="col-md-12">
                    <div class="prev-next">
                        <?php wp_pagenavi(array('query' => $query)); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>